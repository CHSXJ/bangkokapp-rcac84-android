package th.co.ocac.rcac84.di.component;

import android.app.Application;
import android.content.Context;

import th.co.ocac.rcac84.RCAC84;
import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.di.ApplicationContext;
import th.co.ocac.rcac84.di.module.ApplicationModule;
import th.co.ocac.rcac84.service.SyncService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Ch on 05/11/18.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(RCAC84 app);

    void inject(SyncService service);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}