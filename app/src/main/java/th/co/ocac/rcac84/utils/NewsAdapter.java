package th.co.ocac.rcac84.utils;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.News;
import th.co.ocac.rcac84.ui.base.BaseViewHolder;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ch on 22/07/18.
 */

public class NewsAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<News> mLiveNewsResponseList;
//    private final View.OnClickListener mOnClickListener;

    public NewsAdapter(List<News> LiveNewsResponseList) {
        mLiveNewsResponseList = LiveNewsResponseList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_blog_view, parent, false);
//                view.setOnClickListener(mOnClickListener);
                return new ViewHolder(view);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mLiveNewsResponseList != null && mLiveNewsResponseList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mLiveNewsResponseList != null && mLiveNewsResponseList.size() > 0) {
            return mLiveNewsResponseList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<News> NewsList) {
        mLiveNewsResponseList.addAll(NewsList);
        notifyDataSetChanged();
    }

    public News getItemAtIndex(int position) {
        return mLiveNewsResponseList.get(position);
    }

    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.cover_image_view)
        ImageView coverImageView;

        @BindView(R.id.title_text_view)
        TextView titleTextView;

        @BindView(R.id.date_text_view)
        TextView dateTextView;

        @BindView(R.id.content_text_view)
        TextView contentTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
            coverImageView.setImageDrawable(null);
            titleTextView.setText("");
            contentTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final News news = mLiveNewsResponseList.get(position);

            Glide.with(itemView.getContext())
                    .load(news.getImage())
                    .asBitmap()
                    .centerCrop()
                    .into(coverImageView);

            if (news.getName() != null) {
                titleTextView.setText(news.getName());
            }

            if (news.getDate() != null) {
                dateTextView.setText(news.getDate());
            }

            if (news.getText() != null) {
                contentTextView.setText(news.getText());
            }

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

//        @BindView(R.id.btn_retry)
//        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

//        @OnClick(R.id.btn_retry)
//        void onRetryClick() {
//            if (mCallback != null)
//                mCallback.onBlogEmptyViewRetryClick();
//        }
    }
}
