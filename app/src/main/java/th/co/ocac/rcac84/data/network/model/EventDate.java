package th.co.ocac.rcac84.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventDate {

    @Expose
    @SerializedName("instant")
    private String instant;

    @Expose
    @SerializedName("dateStr")
    private String dateStr;

    @Expose
    @SerializedName("agendas")
    private List<Agenda> agendas;

    public String getInstant() {
        return instant;
    }

    public void setInstant(String instant) {
        this.instant = instant;
    }

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public List<Agenda> getAgendas() {
        return agendas;
    }

    public void setAgendas(List<Agenda> agendas) {
        this.agendas = agendas;
    }
}
