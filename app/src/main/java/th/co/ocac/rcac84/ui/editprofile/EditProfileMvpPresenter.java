package th.co.ocac.rcac84.ui.editprofile;

import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.di.PerActivity;
import th.co.ocac.rcac84.ui.base.MvpPresenter;

/**
 * Created by Ch on 16/10/18.
 */

@PerActivity
public interface EditProfileMvpPresenter<V extends EditProfileMvpView> extends MvpPresenter<V> {

    void getLoggedInProfile();

    void saveProfile(User user);
}
