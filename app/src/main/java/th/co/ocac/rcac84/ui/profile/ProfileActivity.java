package th.co.ocac.rcac84.ui.profile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.ui.base.BaseActivity;
import th.co.ocac.rcac84.ui.editprofile.EditProfileActivity;
import th.co.ocac.rcac84.ui.main.MainActivity;
import th.co.ocac.rcac84.utils.PreviousEventAdapter;
import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.jwang123.flagkit.FlagKit;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Ch on 12/10/18.
 */

public class ProfileActivity extends BaseActivity implements ProfileMvpView {

    public static String TAG = "ProfileActivity";

    @Inject
    ProfileMvpPresenter<ProfileMvpView> mPresenter;

    @Inject
    PreviousEventAdapter mEventAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.imageView)
    ImageView profileImageView;

    @BindView(R.id.userName)
    TextView userName;

    @BindView(R.id.age)
    TextView age;

    @BindView(R.id.flag)
    ImageView flag;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private LoginButton txtFbLogin;
    private AccessToken mAccessToken;
    private CallbackManager callbackManager;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ProfileActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(ProfileActivity.this);

        setUp();

    }

    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.getStartIntent(ProfileActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void displayUser(User user) {
//        Log.d(TAG, "Logged in user id: " + user.getNationality());
        Glide.with(this).load(user.getImageUrl()).into(profileImageView);
        userName.setText(user.getFirstName());
        age.setText(user.getBirthDate());
        try {
            flag.setImageDrawable(FlagKit.drawableWithFlag(this, user.getNationality().toLowerCase()));
        } catch (Exception e) {
            // This will catch any exception, because they are all descended from Exception
            System.out.println("Error " + e.getMessage());
            flag.setVisibility(View.GONE);
        }
    }

    @Override
    public void displayPreviousEvent(List<Event> events) {
        mEventAdapter.addItems(events);
    }

    @Override
    public void signedOut() {
        finish();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {

        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mEventAdapter);

        callbackManager = CallbackManager.Factory.create();

        mPresenter.getLoggedInProfile();

    }

    @OnClick(R.id.editProfileBtn)
    public void editProfile() {
        startActivity(EditProfileActivity.getStartIntent(this));
    }

    @OnClick(R.id.signoutBtn)
    public void signingOut() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Sign out?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPresenter.onSignout();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {

            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (upIntent == null) finish();
                else {
                    upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                        // This activity is NOT part of this app's task, so create a new task
                        // when navigating up, with a synthesized back stack.
                        TaskStackBuilder.create(this)
                                // Add all of this activity's parents to the back stack
                                .addNextIntentWithParentStack(upIntent)
                                // Navigate up to the closest parent
                                .startActivities();
                    } else {
                        // This activity is part of this app's task, so simply
                        // navigate up to the logical parent activity.
                        NavUtils.navigateUpTo(this, upIntent);
                    }
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode,  data);
    }

}
