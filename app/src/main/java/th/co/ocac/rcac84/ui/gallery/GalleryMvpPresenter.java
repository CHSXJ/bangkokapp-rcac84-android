package th.co.ocac.rcac84.ui.gallery;

import th.co.ocac.rcac84.ui.base.MvpPresenter;
import th.co.ocac.rcac84.ui.base.MvpView;

import java.util.List;

/**
 * Created by Ch on 22/07/18.
 */

public interface GalleryMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    void getGalleryByTags(List<String> tagIds);

    void getAllTags();
}
