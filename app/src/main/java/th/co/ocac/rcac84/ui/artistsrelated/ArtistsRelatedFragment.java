package th.co.ocac.rcac84.ui.artistsrelated;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.data.network.model.SearchResponse;
import th.co.ocac.rcac84.di.component.ActivityComponent;
import th.co.ocac.rcac84.ui.artist.ArtistAdapter;
import th.co.ocac.rcac84.ui.artist.ArtistDetailActivity;
import th.co.ocac.rcac84.ui.artwork.ArtworkAdapter;
import th.co.ocac.rcac84.ui.artwork.GalleryGridView;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailActivity;
import th.co.ocac.rcac84.ui.base.BaseFragment;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailActivity;
import th.co.ocac.rcac84.utils.PreviousEventAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArtistsRelatedFragment extends BaseFragment implements ArtistsRelatedMvpView, PreviousEventAdapter.Callback {

    private static final String TAG = "ArtistsRelatedFragment";

    @Inject
    ArtistsRelatedMvpPresenter<ArtistsRelatedMvpView> mPresenter;

    @BindView(R.id.artworksGridview)
    GalleryGridView artworksGridview;

    @BindView(R.id.artistsGridview)
    GalleryGridView artistsGridview;

    @BindView(R.id.eventsRecyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.artworksView)
    LinearLayout artworksView;

    @BindView(R.id.artistView)
    LinearLayout artistView;

    @BindView(R.id.eventsView)
    LinearLayout eventsView;

    ArtworkAdapter mArtworkAdapter;
    ArtistAdapter mArtistAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    PreviousEventAdapter eventAdapter;

    static String mArtistId;

    public static ArtistsRelatedFragment newInstance(String artistId) {
        Bundle args = new Bundle();
        ArtistsRelatedFragment fragment = new ArtistsRelatedFragment();
        fragment.setArguments(args);
        mArtistId = artistId;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artist_related, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            eventAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        mPresenter.getArtistsRelated(mArtistId);

        mArtworkAdapter = new ArtworkAdapter(this.getContext());
        artworksGridview.setAdapter(mArtworkAdapter);

        artworksGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                startActivity(ArtworkDetailActivity.getStartIntent(getActivity(), mArtworkAdapter.getItemAtIndex(position).getId()));
            }
        });

        mArtistAdapter = new ArtistAdapter(this.getContext());
        artistsGridview.setAdapter(mArtistAdapter);

        artistsGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                startActivity(ArtistDetailActivity.getStartIntent(getActivity(), mArtistAdapter.getItemAtIndex(position).getId()));
            }
        });

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(eventAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        displayEmpty();
    }

    public void displayEmpty() {
        hideLoading();
        artworksView.setVisibility(View.GONE);
        artistView.setVisibility(View.GONE);
        eventsView.setVisibility(View.GONE);
    }

    @Override
    public void showGallery(List<Artwork> artworks) {
        mArtworkAdapter.addItems(artworks);
    }

    @Override
    public void displayResult(SearchResponse searchResponse) {
        hideLoading();
        Log.d(TAG, String.valueOf(searchResponse));
        if (searchResponse.getArtworks().size() == 0) {
            artworksView.setVisibility(View.GONE);
        } else {
            artworksView.setVisibility(View.VISIBLE);
            mArtworkAdapter.addItems(searchResponse.getArtworks());
        }

        if (searchResponse.getArtists().size() == 0) {
            artistView.setVisibility(View.GONE);
        } else {
            artistView.setVisibility(View.VISIBLE);
            mArtistAdapter.addItems(searchResponse.getArtists());
        }

        if (searchResponse.getEvents().size() == 0) {
            eventsView.setVisibility(View.GONE);
        } else {
            eventsView.setVisibility(View.VISIBLE);
            eventAdapter.addItems(searchResponse.getEvents());
        }
    }

    @Override
    public void onEventClick(String id) {
        Log.d(TAG, "openEventDetail " + id);
        startActivity(EventDetailActivity.getStartIntent(getActivity(), id));
    }
}
