package th.co.ocac.rcac84.utils;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import th.co.ocac.rcac84.R;

import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.EventDate;
import th.co.ocac.rcac84.data.network.model.MediaObj;
import th.co.ocac.rcac84.data.network.model.Tag;
import th.co.ocac.rcac84.ui.base.BaseViewHolder;
import com.bumptech.glide.Glide;
import com.cunoraz.tagview.TagView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ch on 22/07/18.
 */

public class WhatsnewEventAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;


    private Callback mCallback;
    private List<Event> mLiveEventsResponseList;
//    private final View.OnClickListener mOnClickListener;

    public WhatsnewEventAdapter(List<Event> LiveEventsResponseList) {
        mLiveEventsResponseList = LiveEventsResponseList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {

            case VIEW_TYPE_NORMAL:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_view, parent, false);
                                if (getItemCount()>1) {
                                    view.setLayoutParams(new ViewGroup.LayoutParams((int) (parent.getWidth() * 0.95), ViewGroup.LayoutParams.WRAP_CONTENT));
                                }
                return new ViewHolder(view);

            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (mLiveEventsResponseList != null && mLiveEventsResponseList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mLiveEventsResponseList != null && mLiveEventsResponseList.size() > 0) {
            return mLiveEventsResponseList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<Event> eventList) {
        mLiveEventsResponseList.addAll(eventList);
        notifyDataSetChanged();
    }

    public Event getItemAtIndex(int position) {
        return mLiveEventsResponseList.get(position);
    }

    public interface Callback {

        void onBlogEmptyViewRetryClick();

        void openEventDetail(String eventId);

        void onJoinEvent(String id);

        void onDisplayAgenda(List<EventDate> eventId);
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.cover_image_view)
        ImageView coverImageView;

        @BindView(R.id.title_text_view)
        TextView titleTextView;

        @BindView(R.id.date_text_view)
        TextView dateTextView;

        @BindView(R.id.content_text_view)
        TextView contentTextView;

        @BindView(R.id.joinEventBtn)
        Button joinBtn;

        @BindView(R.id.tagListView)
        TagView tagListView;

        @BindView(R.id.agendaBtn)
        ImageButton agendaBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
            coverImageView.setImageDrawable(null);
            titleTextView.setText("");
            contentTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final Event event = mLiveEventsResponseList.get(position);

            if (event.getMediaList().size() > 0) {
                MediaObj firstMedia = event.getMediaList().get(0);
                Glide.with(itemView.getContext())
                        .load(firstMedia.getUrl())
                        .asBitmap()
                        .centerCrop()
                        .into(coverImageView);
            }

            if (event.getName() != null) {
                titleTextView.setText(event.getName());
            }

            if (event.getDateString() != null) {
                dateTextView.setText(event.getDateString());
            }

            if (event.getAbout() != null) {
                contentTextView.setText(event.getAbout());
            }

            for (final Tag tag: event.getTags()) {
                com.cunoraz.tagview.Tag t = new com.cunoraz.tagview.Tag(tag.getName());
                t.layoutColor = ContextCompat.getColor(itemView.getContext(),R.color.colorAccent);
                t.tagTextColor = Color.BLACK;
                t.radius = 15;

                tagListView.addTag(t);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (event.getId() != null) {
                        mCallback.openEventDetail(event.getId());
                    }
//                    mCallbackstartActivity(EventDetailActivity.getStartIntent(getActivity(), mEventAdapter.getItemAtIndex(position).getId()));
                }
            });

            joinBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (event.getId() != null) {
                        mCallback.onJoinEvent(event.getId());
                    }
                }
            });

            agendaBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (event.getDateList() != null) {
                        mCallback.onDisplayAgenda(event.getDateList());
                    }
                }
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

//        @BindView(R.id.btn_retry)
//        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

//        @OnClick(R.id.btn_retry)
//        void onRetryClick() {
//            if (mCallback != null)
//                mCallback.onBlogEmptyViewRetryClick();
//        }
    }
}
