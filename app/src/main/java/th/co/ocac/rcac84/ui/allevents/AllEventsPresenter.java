package th.co.ocac.rcac84.ui.allevents;

import com.androidnetworking.error.ANError;
import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.JoinResponse;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ch on 22/07/18.
 */

public class AllEventsPresenter<V extends AllEventsMvpView> extends BasePresenter<V> implements
        AllEventsMvpPresenter<V> {

    private static final String TAG = "WhatsnewPresenter";

    @Inject
    public AllEventsPresenter(DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onCurrentEventViewPrepared() {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getCurrentEventsApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Event>>() {
                    @Override
                    public void accept(@NonNull List<Event> eventResponse)
                            throws Exception {
                        if (eventResponse != null) {
                            getMvpView().updateCurrentEvent(eventResponse);
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public void onNextEventViewPrepared() {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getNextEventsApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Event>>() {
                    @Override
                    public void accept(@NonNull List<Event> eventResponse)
                            throws Exception {
                        if (eventResponse != null) {
                            getMvpView().updateNextEvent(eventResponse);
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public String getUserToken() {
        if (getDataManager().getAccessToken() != null) {
            return getDataManager().getAccessToken();
        } else {
            return null;
        }

    }

    @Override
    public void joinEvent(final String eventId) {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getJoinEventApiCall(eventId, getDataManager().getUserInfo().getId())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<JoinResponse>() {
                    @Override
                    public void accept(JoinResponse joinResponse) throws Exception {
                        getMvpView().showJoinAlert(eventId, "Complete!, successfully join this event");
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getMvpView().showJoinAlert(eventId, "You've already registered");
                    }
                }));
    }
}


