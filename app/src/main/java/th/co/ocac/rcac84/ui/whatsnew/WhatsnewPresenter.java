package th.co.ocac.rcac84.ui.whatsnew;

import com.androidnetworking.error.ANError;
import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.JoinResponse;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ch on 22/07/18.
 */

public class WhatsnewPresenter<V extends WhatsnewMvpView> extends BasePresenter<V> implements
        WhatsnewMvpPresenter<V> {

    private static final String TAG = "WhatsnewPresenter";

    @Inject
    public WhatsnewPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    public void loadCurrentEvents() {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getCurrentEventsApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Event>>() {
                    @Override
                    public void accept(@NonNull List<Event> eventResponse)
                            throws Exception {
                        if (eventResponse != null) {
                            getMvpView().updateEvent(eventResponse);
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public void loadArtOfTheMonth() {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getArtOfTheMonthApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Artwork>>() {
                    @Override
                    public void accept(List<Artwork> artworks) throws Exception {
                        if (artworks != null) {
                            getMvpView().updateArtworks(artworks);
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public String getUserToken() {
        if (getDataManager().getCurrentUserLoggedInMode() != DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType()) {
            if (getDataManager().getAccessToken() != null) {
                return getDataManager().getAccessToken();
            } else {
                return null;
            }
        } else {
            return null;
        }

    }

    @Override
    public void joinEvent(final String eventId) {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
            .getJoinEventApiCall(eventId, getDataManager().getUserInfo().getId())
            .subscribeOn(getSchedulerProvider().io())
            .observeOn(getSchedulerProvider().ui())
            .subscribe(new Consumer<JoinResponse>() {
                @Override
                public void accept(JoinResponse joinResponse) throws Exception {
                    getMvpView().showJoinAlert(eventId, "Complete!, successfully join this event");
                }
            }, new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {
                    getMvpView().showJoinAlert(eventId, "You've already registered ");
                }
            }));
    }


}


