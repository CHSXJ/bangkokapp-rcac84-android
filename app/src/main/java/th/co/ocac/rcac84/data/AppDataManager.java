package th.co.ocac.rcac84.data;


import android.content.Context;

import th.co.ocac.rcac84.data.network.model.Artist;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.JoinResponse;
import th.co.ocac.rcac84.data.network.model.Museum;
import th.co.ocac.rcac84.data.network.model.News;
import th.co.ocac.rcac84.data.network.model.SearchResponse;
import th.co.ocac.rcac84.data.network.model.Tag;
import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.data.db.DbHelper;
import th.co.ocac.rcac84.data.db.model.Option;
import th.co.ocac.rcac84.data.db.model.Question;
import th.co.ocac.rcac84.data.network.ApiHeader;
import th.co.ocac.rcac84.data.network.ApiHelper;
import th.co.ocac.rcac84.data.network.model.LoginRequest;
import th.co.ocac.rcac84.data.network.model.LoginResponse;
import th.co.ocac.rcac84.data.network.model.LogoutResponse;
import th.co.ocac.rcac84.data.network.model.OpenSourceResponse;
import th.co.ocac.rcac84.data.prefs.PreferencesHelper;
import th.co.ocac.rcac84.di.ApplicationContext;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by Ch on 18/10/18.
 */

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final DbHelper mDbHelper;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          DbHelper dbHelper,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest
                                                                request) {
        return mApiHelper.doFacebookLoginApiCall(request);
    }

    @Override
    public Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest
                                                              request) {
        return mApiHelper.doServerLoginApiCall(request);
    }

    @Override
    public Single<LogoutResponse> doLogoutApiCall() {
        return mApiHelper.doLogoutApiCall();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public User getCurrentUser() {
        return mPreferencesHelper.getCurrentUser();
    }

    @Override
    public void setCurrentUser(User user) {
        mPreferencesHelper.setCurrentUser(user);
    }

    @Override
    public void updateApiHeader(String accessToken) {
//        mApiHelper.getApiHeader().getProtectedApiHeader().setUserId(userId);
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

//    @Override
//    public void updateUserInfo(
//            String accessToken,
//            Long userId,
//            LoggedInMode loggedInMode,
//            String userName,
//            String email,
//            String profilePicPath) {
//
//        setAccessToken(accessToken);
//        setCurrentUser(userId);
//        setCurrentUserLoggedInMode(loggedInMode);
//        setCurrentUserName(userName);
//        setCurrentUserEmail(email);
//        setCurrentUserProfilePicUrl(profilePicPath);
//
//        updateApiHeader(accessToken);
//    }


    @Override
    public void updateUserInfo(User user, LoggedInMode loggedInMode) {
        setCurrentUser(user);
        setCurrentUserLoggedInMode(loggedInMode);
    }

    @Override
    public void setUserAsLoggedOut() {
        setAccessToken("");
        updateUserInfo(null, LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT);
    }

    @Override
    public User getUserInfo() {
        return mPreferencesHelper.getCurrentUser();
    }

    @Override
    public Observable<Boolean> isQuestionEmpty() {
        return mDbHelper.isQuestionEmpty();
    }

    @Override
    public Observable<Boolean> isOptionEmpty() {
        return mDbHelper.isOptionEmpty();
    }

    @Override
    public Observable<Boolean> saveQuestion(Question question) {
        return mDbHelper.saveQuestion(question);
    }

    @Override
    public Observable<Boolean> saveOption(Option option) {
        return mDbHelper.saveOption(option);
    }

    @Override
    public Observable<Boolean> saveQuestionList(List<Question> questionList) {
        return mDbHelper.saveQuestionList(questionList);
    }

    @Override
    public Observable<Boolean> saveOptionList(List<Option> optionList) {
        return mDbHelper.saveOptionList(optionList);
    }

    @Override
    public Observable<List<Question>> getAllQuestions() {
        return mDbHelper.getAllQuestions();
    }

    @Override
    public Observable<List<Event>> getCurrentEventsApiCall() {
        return mApiHelper.getCurrentEventsApiCall();
    }

    @Override
    public Observable<List<Event>> getNextEventsApiCall() {
        return mApiHelper.getNextEventsApiCall();
    }

    @Override
    public Single<OpenSourceResponse> getOpenSourceApiCall() {
        return mApiHelper.getOpenSourceApiCall();
    }

    @Override
    public Observable<List<Artwork>> getArtOfTheMonthApiCall() {
        return mApiHelper.getArtOfTheMonthApiCall();
    }

    @Override
    public Observable<List<Artwork>> getGalleryByTagsApiCall(List<String> tagIds) {
        return mApiHelper.getGalleryByTagsApiCall(tagIds);
    }

    @Override
    public Single<Event> getEventDetail(String eventId) {
        return mApiHelper.getEventDetail(eventId);
    }

    @Override
    public Single<Artwork> getArtworkDetail(String mArtworkId) {
        return mApiHelper.getArtworkDetail(mArtworkId);
    }

    @Override
    public Single<Artist> getArtistDetail(String mArtistId) {
        return mApiHelper.getArtistDetail(mArtistId);
    }

    @Override
    public Observable<List<Artwork>> getArtworksByArtistIdApiCall(String artistId) {
        return  mApiHelper.getArtworksByArtistIdApiCall(artistId);
    }

    @Override
    public Single<User> getUserProfileApiCall() {
        return mApiHelper.getUserProfileApiCall();
    }

    @Override
    public Observable<List<Event>> getUserPreviousJoinedApiCall(String userId) {
        return mApiHelper.getUserPreviousJoinedApiCall(userId);
    }

    @Override
    public Single<String> getSaveProfileApiCall(User user) {
        return mApiHelper.getSaveProfileApiCall(user);
    }

    @Override
    public Single<JoinResponse> getJoinEventApiCall(String eventId, String userId) {
        return mApiHelper.getJoinEventApiCall(eventId, userId);
    }

    @Override
    public Single<SearchResponse> getSearchApiCall(String s) {
        return mApiHelper.getSearchApiCall(s);
    }

    @Override
    public Observable<List<News>> getNewsApiCall() {
        return mApiHelper.getNewsApiCall();
    }

    @Override
    public Single<Museum> getMuseumApiCall(String s) {
        return  mApiHelper.getMuseumApiCall(s);
    }

    @Override
    public Observable<List<Tag>> getAllTagsApiCall() {
        return mApiHelper.getAllTagsApiCall();
    }

    @Override
    public Single<String> getRegisterProfileApiCall(User user) {
        return mApiHelper.getRegisterProfileApiCall(user);
    }

    @Override
    public Single<SearchResponse> getArtistsRelatedApiCall(String artistId) {
        return mApiHelper.getArtistsRelatedApiCall(artistId);
    }
}
