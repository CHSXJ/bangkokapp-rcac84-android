package th.co.ocac.rcac84.data;


import th.co.ocac.rcac84.data.db.DbHelper;
import th.co.ocac.rcac84.data.network.ApiHelper;
import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.data.prefs.PreferencesHelper;

/**
 * Created by Ch on 10/10/18.
 */

public interface DataManager extends DbHelper, PreferencesHelper, ApiHelper {

    void updateApiHeader(String accessToken);

    void updateUserInfo(User user, LoggedInMode loggedInMode);

    void setUserAsLoggedOut();

    User getUserInfo();

//    Observable<Boolean> seedDatabaseQuestions();
//
//    Observable<Boolean> seedDatabaseOptions();

//    void updateUserInfo(
//            String accessToken,
//            Long userId,
//            LoggedInMode loggedInMode,
//            String userName,
//            String email,
//            String profilePicPath);

    enum LoggedInMode {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_LOGGED_IN(1);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }
}
