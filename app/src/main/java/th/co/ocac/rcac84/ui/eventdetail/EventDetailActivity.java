package th.co.ocac.rcac84.ui.eventdetail;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.MediaObj;
import th.co.ocac.rcac84.ui.about.WebviewActivity;
import th.co.ocac.rcac84.ui.artist.ArtistDetailActivity;
import th.co.ocac.rcac84.ui.artwork.SampleAdapter;
import th.co.ocac.rcac84.ui.artwork.GalleryGridView;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailActivity;
import th.co.ocac.rcac84.ui.base.BaseActivity;
import com.bumptech.glide.Glide;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventDetailActivity extends BaseActivity implements EventDetailMvpView {

    @Inject
    EventDetailMvpPresenter<EventDetailMvpView> mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

//    @BindView(R.id.imageView)
//    ImageView imageView;

    @BindView(R.id.carouselView)
    CarouselView carouselView;

    @BindView(R.id.play_button)
    ImageView playButton;

    @BindView(R.id.eventName)
    TextView eventName;

    @BindView(R.id.eventDetail)
    TextView eventDetail;

    @BindView(R.id.eventDates)
    TextView eventDates;

    String mEventId;

    SampleAdapter mArtworkAdapter;
    SampleAdapter mArtistAdapter;

    @BindView(R.id.artworksGridview)
    GalleryGridView artworksGridview;

    @BindView(R.id.artistsGridview)
    GalleryGridView artistsGridview;

    @BindView(R.id.scrollView)
    ScrollView scrollView;
//
//    @BindView(R.id.feed_view_pager)
//    ViewPager mViewPager;
//
//    @BindView(R.id.tab_layout)
//    TabLayout mTabLayout;

    public static Intent getStartIntent(Context context, String eventId) {
        Intent intent = new Intent(context, EventDetailActivity.class);
        intent.putExtra("eventId", eventId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        Intent intent = getIntent();
        mEventId = intent.getStringExtra("eventId");
        mPresenter.onEventDetailPrepared(mEventId);
        setUp();
    }

    @Override
    protected void setUp() {

        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        //Setup sample ARTWORKS
        mArtworkAdapter = new SampleAdapter(getApplication());
        artworksGridview.setAdapter(mArtworkAdapter);

        artworksGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                startActivity(ArtworkDetailActivity.getStartIntent(getApplication(), mArtworkAdapter.getItemAtIndex(position).getId()));
            }
        });

        //Setup sample ARTISTS
        mArtistAdapter = new SampleAdapter(getApplication());
        artistsGridview.setAdapter(mArtistAdapter);

        artistsGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                startActivity(ArtistDetailActivity.getStartIntent(getApplication(), mArtistAdapter.getItemAtIndex(position).getId()));
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (upIntent == null) finish();
                else {
                    upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                        // This activity is NOT part of this app's task, so create a new task
                        // when navigating up, with a synthesized back stack.
                        TaskStackBuilder.create(this)
                                // Add all of this activity's parents to the back stack
                                .addNextIntentWithParentStack(upIntent)
                                // Navigate up to the closest parent
                                .startActivities();
                    } else {
                        // This activity is part of this app's task, so simply
                        // navigate up to the logical parent activity.
                        NavUtils.navigateUpTo(this, upIntent);
                    }
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void updateEventDetailView(final Event event) {

//        Glide.with(getApplicationContext())
//                .load(Uri.parse(event.getMediaList().get(0).getUrl()))
//                .into(imageView);
        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                if (event.getMediaList().get(position).getType() == MediaObj.MediaType.IMAGE) {
                    Glide.with(getApplicationContext())
                            .load(Uri.parse(event.getMediaList().get(position).getUrl()))
                            .into(imageView);
                } else {
                    Glide.with(getApplicationContext())
                            .load(Uri.parse(event.getMediaList().get(position).getThumbUrl()))
                            .into(imageView);
                }
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        });
        carouselView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (event.getMediaList().get(position).getType() == MediaObj.MediaType.VIDEO) {
                    playButton.setVisibility(View.VISIBLE);
                } else if (event.getMediaList().get(position).getType() == MediaObj.MediaType.YOUTUBE) {
                    playButton.setVisibility(View.VISIBLE);
                } else {
                    playButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        carouselView.setPageCount(event.getMediaList().size());
        carouselView.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(int position) {
                if (event.getMediaList().get(position).getType() != MediaObj.MediaType.IMAGE) {
                    startActivity(WebviewActivity.getStartIntent(EventDetailActivity.this, event.getMediaList().get(position).getUrl()));
                }
            }
        });
        eventName.setText(event.getName());
        eventDates.setText(event.getDateString());
        eventDetail.setText(event.getAbout());
        mArtworkAdapter.addItems(event.getSampleArtworks());
        mArtistAdapter.addItems(event.getSampleArtists());

        scrollView.smoothScrollTo(0,0);

    }
}