package th.co.ocac.rcac84.data.network;

import th.co.ocac.rcac84.data.network.model.Artist;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.JoinResponse;
import th.co.ocac.rcac84.data.network.model.LoginRequest;
import th.co.ocac.rcac84.data.network.model.LoginResponse;
import th.co.ocac.rcac84.data.network.model.LogoutResponse;
import th.co.ocac.rcac84.data.network.model.Museum;
import th.co.ocac.rcac84.data.network.model.News;
import th.co.ocac.rcac84.data.network.model.OpenSourceResponse;
import th.co.ocac.rcac84.data.network.model.SearchResponse;
import th.co.ocac.rcac84.data.network.model.Tag;
import th.co.ocac.rcac84.data.network.model.User;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by Ch on 10/10/17.
 */

public interface ApiHelper {

    ApiHeader getApiHeader();

//    Single<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest request);

    Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest request);

    Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest request);

    Single<LogoutResponse> doLogoutApiCall();

    io.reactivex.Observable<List<Event>> getCurrentEventsApiCall();

    io.reactivex.Observable<List<Event>> getNextEventsApiCall();

    Single<OpenSourceResponse> getOpenSourceApiCall();

    Observable<List<Artwork>> getArtOfTheMonthApiCall();

    Observable<List<Artwork>> getGalleryByTagsApiCall(List<String> tagIds);

    Single<Event> getEventDetail(String eventId);

    Single<Artwork> getArtworkDetail(String mArtworkId);

    Single<Artist> getArtistDetail(String mArtistId);

    Observable<List<Artwork>> getArtworksByArtistIdApiCall(String artistId);

    Single<User> getUserProfileApiCall();

    Observable<List<Event>> getUserPreviousJoinedApiCall(String userId);

    Single<String> getSaveProfileApiCall(User user);

    Single<JoinResponse> getJoinEventApiCall(String eventId, String userId);

    Single<SearchResponse> getSearchApiCall(String s);

    Observable<List<News>> getNewsApiCall();

    Single<Museum> getMuseumApiCall(String s);

    Observable<List<Tag>> getAllTagsApiCall();

    Single<String> getRegisterProfileApiCall(User user);

    Single<SearchResponse> getArtistsRelatedApiCall(String artistId);
}
