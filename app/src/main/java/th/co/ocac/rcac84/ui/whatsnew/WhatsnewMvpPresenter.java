package th.co.ocac.rcac84.ui.whatsnew;

import th.co.ocac.rcac84.ui.base.MvpPresenter;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 22/07/18.
 */

public interface WhatsnewMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    void loadCurrentEvents();

    void loadArtOfTheMonth();

    String getUserToken();

    void joinEvent(String eventId);
}
