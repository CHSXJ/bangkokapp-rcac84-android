package th.co.ocac.rcac84.ui.artist;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import th.co.ocac.rcac84.data.network.model.Artist;
import th.co.ocac.rcac84.ui.artistsartworks.ArtistsArtworksFragment;
import th.co.ocac.rcac84.ui.artistsrelated.ArtistsRelatedFragment;

/**
 * Created by Ch on 21/07/2018.
 */

public class ArtistDetailPagerAdapter extends FragmentStatePagerAdapter {

    private int mTabCount;
    private Artist mArtist;

    public ArtistDetailPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.mTabCount = 0;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return ArtistProfileFragment.newInstance(mArtist);
            case 1:
                return ArtistsArtworksFragment.newInstance(mArtist.getId());
            case 2:
                return ArtistsRelatedFragment.newInstance(mArtist.getId());
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mTabCount;
    }

    public void setCount(int count) {
        mTabCount = count;
    }

    public Artist getmArtist() {
        return mArtist;
    }

    public void setmArtist(Artist mArtist) {
        this.mArtist = mArtist;
    }
}
