package th.co.ocac.rcac84.ui.about;

import th.co.ocac.rcac84.data.network.model.Museum;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 21/10/18.
 */

public interface AboutMvpView extends MvpView {

    void displayMuseum(Museum museum);
}
