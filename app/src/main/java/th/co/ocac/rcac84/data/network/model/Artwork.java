package th.co.ocac.rcac84.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Artwork {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("artOfMonth")
    private boolean artOfMonth;

    @Expose
    @SerializedName("artist")
    private Artist artist;

    @Expose
    @SerializedName("events")
    private List<Event> events;

    @Expose
    @SerializedName("artistId")
    private String artistId;

    @Expose
    @SerializedName("concept")
    private String concept;

    @Expose
    @SerializedName("image")
    private String image;

    @Expose
    @SerializedName("size")
    private String size;

    @Expose
    @SerializedName("tags")
    private List<Tag> tags;

    @Expose
    @SerializedName("technique")
    private String technique;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isArtOfMonth() {
        return artOfMonth;
    }

    public void setArtOfMonth(boolean artOfMonth) {
        this.artOfMonth = artOfMonth;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getTechnique() {
        return technique;
    }

    public void setTechnique(String technique) {
        this.technique = technique;
    }
}
