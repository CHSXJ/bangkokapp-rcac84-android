package th.co.ocac.rcac84.ui.eventdetail;

import th.co.ocac.rcac84.ui.base.MvpPresenter;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 22/07/18.
 */

public interface EventDetailMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    void onEventDetailPrepared(String mEventId);
}
