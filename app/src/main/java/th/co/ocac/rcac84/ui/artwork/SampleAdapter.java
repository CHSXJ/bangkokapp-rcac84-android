package th.co.ocac.rcac84.ui.artwork;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import th.co.ocac.rcac84.data.network.model.Sample;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class SampleAdapter extends BaseAdapter {

    private Context mContext;
    private List<Sample> items;

    public SampleAdapter(Context c) {
        mContext = c;
        items = new ArrayList<Sample>();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public Sample getItemAtIndex(int position) {
        return items.get(position);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ImageView imageView;
        if (view == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(viewGroup.getWidth()/3, viewGroup.getWidth()/3));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) view;
        }

        Glide.with(mContext).load(items.get(i)
                            .getImageURL()).into(imageView);
        return imageView;
    }

    public void addItems(List<Sample> samples) {
        items.addAll(samples);
        notifyDataSetChanged();
    }


//    private Integer[] mThumbIds = {
//            R.drawable.dm1, R.drawable.dm2,
//            R.drawable.dm3, R.drawable.dm4,
//            R.drawable.dm5, R.drawable.dm6,
//            R.drawable.dm7, R.drawable.dm8,
//            R.drawable.dm9, R.drawable.dm10,
//            R.drawable.dm11, R.drawable.dm12
//    };

}
