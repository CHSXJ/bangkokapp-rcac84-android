package th.co.ocac.rcac84.utils;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.ui.base.BaseViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ch on 22/07/18.
 */

public class PreviousEventAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<Event> mLiveEventsResponseList;
//    private final View.OnClickListener mOnClickListener;

    public PreviousEventAdapter(List<Event> LiveEventsResponseList) {
        mLiveEventsResponseList = LiveEventsResponseList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_previous_event_view, parent, false);
                return new ViewHolder(view);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mLiveEventsResponseList != null && mLiveEventsResponseList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mLiveEventsResponseList != null && mLiveEventsResponseList.size() > 0) {
            return mLiveEventsResponseList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<Event> eventList) {
        mLiveEventsResponseList.addAll(eventList);
        notifyDataSetChanged();
    }

    public Event getItemAtIndex(int position) {
        return mLiveEventsResponseList.get(position);
    }

    public interface Callback {
        void onEventClick(String id);
    }

    public class ViewHolder extends BaseViewHolder {

//        @BindView(R.id.cover_image_view)
//        ImageView coverImageView;

        @BindView(R.id.title_text_view)
        TextView titleTextView;

        @BindView(R.id.date_text_view)
        TextView dateTextView;

        @BindView(R.id.content_text_view)
        TextView contentTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
            titleTextView.setText("");
            contentTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final Event event = mLiveEventsResponseList.get(position);

//            if (event.getMediaList().size() > 0) {
//                MediaObj firstMedia = event.getMediaList().get(0);
//                Glide.with(itemView.getContext())
//                        .load(firstMedia.getUrl())
//                        .asBitmap()
//                        .centerCrop()
//                        .into(coverImageView);
//            }

            if (event.getName() != null) {
                titleTextView.setText(event.getName());
            }

            if (event.getDateString() != null) {
                dateTextView.setText(event.getDateString());
            }

            if (event.getAbout() != null) {
                contentTextView.setText(event.getAbout());
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (event.getId() != null) {
                        mCallback.onEventClick(event.getId());
                    }
                }
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

//        @BindView(R.id.btn_retry)
//        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

//        @OnClick(R.id.btn_retry)
//        void onRetryClick() {
//            if (mCallback != null)
//                mCallback.onEventClick(event.getId());
//        }
    }
}
