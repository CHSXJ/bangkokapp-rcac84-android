package th.co.ocac.rcac84.ui.search;

import th.co.ocac.rcac84.ui.base.MvpPresenter;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 22/07/18.
 */

public interface SearchMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    void onViewPrepared();

    void searching(String s);
}
