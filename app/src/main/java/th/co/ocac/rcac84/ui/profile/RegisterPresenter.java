package th.co.ocac.rcac84.ui.profile;

import com.androidnetworking.error.ANError;
import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.LoginRequest;
import th.co.ocac.rcac84.data.network.model.LoginResponse;
import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.CommonUtils;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ch on 10/10/18.
 */

public class RegisterPresenter<V extends RegisterMvpView> extends BasePresenter<V>
        implements RegisterMvpPresenter<V> {

    private static final String TAG = "RegisterPresenter";

    @Inject
    public RegisterPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    public void getUserProfile() {
        getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager()
            .getUserProfileApiCall()
            .subscribeOn(getSchedulerProvider().io())
            .observeOn(getSchedulerProvider().ui())
            .subscribe(new Consumer<User>() {
                @Override
                public void accept(User user) throws Exception {
                    getDataManager().updateUserInfo(user, DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_IN);
                    getMvpView().openMainActivity();
                }
            }, new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {

                }
            }));
    }

    @Override
    public void getLoggedInProfile() {
        getMvpView().displayUser(getDataManager().getUserInfo());
    }

    @Override
    public void register(final User user) {

        getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager()
            .getRegisterProfileApiCall(user)
            .subscribeOn(getSchedulerProvider().io())
            .observeOn(getSchedulerProvider().ui())
            .subscribe(new Consumer<String>() {
                @Override
                public void accept(String string) throws Exception {
                    onServerLoginClick(user.getEmail(), "");
                }

            }, new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {
                    System.out.println("throwable = " + throwable);
                    getMvpView().showMessage("There's something wrong, try again later");
                }
            }));
    }

    public void onServerLoginClick(String email, String password) {
        //validate email and password
        if (email == null || email.isEmpty()) {
            getMvpView().onError(R.string.empty_email);
            return;
        }
        if (!CommonUtils.isEmailValid(email)) {
            getMvpView().onError(R.string.invalid_email);
            return;
        }
//        if (password == null || password.isEmpty()) {
//            getMvpView().onError(R.string.empty_password);
//            return;
//        }
        getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .doServerLoginApiCall(new LoginRequest.ServerLoginRequest(email, password))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<LoginResponse>() {
                    @Override
                    public void accept(LoginResponse response) throws Exception {

                        getDataManager().setAccessToken(response.getAccessToken());
                        getDataManager().updateApiHeader(response.getAccessToken());
                        getUserProfile();

//                        getDataManager().updateUserInfo(
//                                response.getAccessToken(),
//                                response.getUserId(),
//                                DataManager.LoggedInMode.LOGGED_IN_MODE_SERVER,
//                                response.getUserName(),
//                                response.getUserEmail(),
//                                response.getGoogleProfilePicUrl());

                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();
                        getMvpView().showMessage("Already registered");
                        getMvpView().openMainActivity();

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the login error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                            getMvpView().showMessage("There's something wrong, try again later");
                            getMvpView().openMainActivity();
                        }
                    }
                }));
    }
}
