/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package th.co.ocac.rcac84.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.di.ApplicationContext;
import th.co.ocac.rcac84.di.PreferenceInfo;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_CURRENT_USER = "PREF_KEY_CURRENT_USER";

//    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
//    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
//    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";
//    private static final String PREF_KEY_CURRENT_USER_PROFILE_PIC_URL
//            = "PREF_KEY_CURRENT_USER_PROFILE_PIC_URL";
    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }
//
//    @Override
//    public Long getCurrentUserId() {
//        long userId = mPrefs.getLong(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX);
//        return userId == AppConstants.NULL_INDEX ? null : userId;
//    }

    @Override
    public User getCurrentUser() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_KEY_CURRENT_USER, "");
        User obj = gson.fromJson(json, User.class);

        return  obj;
    }

    @Override
    public void setCurrentUser(User user) {
//        mPrefs.edit().put(PREF_KEY_CURRENT_USER_ID, user).apply();
//        mPrefs.edit().putLong(PREF_KEY_CURRENT_USER_ID, id).apply();

        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString(PREF_KEY_CURRENT_USER, json);
        prefsEditor.apply();

    }

//    @Override
//    public String getCurrentUserName() {
//        return mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, null);
//    }
//
//    @Override
//    public void setCurrentUserName(String userName) {
//        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, userName).apply();
//    }
//
//    @Override
//    public String getCurrentUserEmail() {
//        return mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, null);
//    }
//
//    @Override
//    public void setCurrentUserEmail(String email) {
//        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_EMAIL, email).apply();
//    }
//
//    @Override
//    public String getCurrentUserProfilePicUrl() {
//        return mPrefs.getString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, null);
//    }
//
//    @Override
//    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
//        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, profilePicUrl).apply();
//    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPrefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType());
    }

    @Override
    public void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode) {
        mPrefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.getType()).apply();
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }
}
