package th.co.ocac.rcac84.ui.profile;

import android.util.Log;

import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ch on 10/10/18.
 */

public class ProfilePresenter<V extends ProfileMvpView> extends BasePresenter<V>
        implements ProfileMvpPresenter<V> {

    private static final String TAG = "ProfilePresenter";

    @Inject
    public ProfilePresenter(DataManager dataManager,
                            SchedulerProvider schedulerProvider,
                            CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    public void getUserProfile() {
        getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager()
            .getUserProfileApiCall()
            .subscribeOn(getSchedulerProvider().io())
            .observeOn(getSchedulerProvider().ui())
            .subscribe(new Consumer<User>() {
                @Override
                public void accept(User user) throws Exception {
                    getDataManager().updateUserInfo(user, DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_IN);
//                    getMvpView().openMainActivity();
                    getMvpView().displayUser(user);
                }
            }, new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {

                }
            }));
    }

    @Override
    public void getLoggedInProfile() {
        if (getDataManager().getCurrentUserLoggedInMode() == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_IN.ordinal()) {
            getMvpView().displayUser(getDataManager().getUserInfo());
            getUserPreviousJoined(getDataManager().getUserInfo().getId());
            Log.d(TAG, "user "+ getDataManager().getUserInfo());
        } else {
            getUserProfile();
        }
    }

    @Override
    public void onSignout() {
        getDataManager().setUserAsLoggedOut();
        getMvpView().signedOut();
    }

    public void getUserPreviousJoined(String userId){
        getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager()
            .getUserPreviousJoinedApiCall(userId)
            .subscribeOn(getSchedulerProvider().io())
            .observeOn(getSchedulerProvider().ui())
            .subscribe(new Consumer<List<Event>>() {
                @Override
                public void accept(List<Event> events) throws Exception {
                    Log.d(TAG, events.toString());
                    getMvpView().displayPreviousEvent(events);
                    getMvpView().hideLoading();
                }
            }, new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {

                }
            }));
    }

}
