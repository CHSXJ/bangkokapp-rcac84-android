package th.co.ocac.rcac84.ui.artistsrelated;

import com.androidnetworking.error.ANError;
import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.SearchResponse;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ch on 22/07/18.
 */

public class ArtistsRelatedPresenter<V extends ArtistsRelatedMvpView> extends BasePresenter<V> implements
        ArtistsRelatedMvpPresenter<V> {

    private static final String TAG = "ArtistsRelatedPresenter";

    @Inject
    public ArtistsRelatedPresenter(DataManager dataManager,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getArtistsRelated(String artistId) {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getArtistsRelatedApiCall(artistId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<SearchResponse>() {
                    @Override
                    public void accept(SearchResponse searchResponse) throws Exception {
                        getMvpView().displayResult(searchResponse);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getMvpView().displayEmpty();
                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));

    }
}


