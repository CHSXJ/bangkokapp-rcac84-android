package th.co.ocac.rcac84.di.component;

import th.co.ocac.rcac84.di.PerActivity;
import th.co.ocac.rcac84.di.module.ActivityModule;
import th.co.ocac.rcac84.ui.about.AboutActivity;
import th.co.ocac.rcac84.ui.about.WebviewActivity;
import th.co.ocac.rcac84.ui.artist.ArtistDetailActivity;
import th.co.ocac.rcac84.ui.artist.ArtistProfileFragment;
import th.co.ocac.rcac84.ui.artistsartworks.ArtistsArtworksFragment;
import th.co.ocac.rcac84.ui.artistsrelated.ArtistsRelatedFragment;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailActivity;
import th.co.ocac.rcac84.ui.allevents.AllEventsFragment;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailActivity;
import th.co.ocac.rcac84.ui.location.LocationActivity;
import th.co.ocac.rcac84.ui.agendaDialog.AgendaDialog;
import th.co.ocac.rcac84.ui.news.NewsActivity;
import th.co.ocac.rcac84.ui.gallery.GalleryFragment;
import th.co.ocac.rcac84.ui.login.LoginActivity;
import th.co.ocac.rcac84.ui.main.MainActivity;
import th.co.ocac.rcac84.ui.editprofile.EditProfileActivity;
import th.co.ocac.rcac84.ui.profile.ProfileActivity;
import th.co.ocac.rcac84.ui.profile.RegisterActivity;
import th.co.ocac.rcac84.ui.search.SearchFragment;
import th.co.ocac.rcac84.ui.splash.SplashActivity;
import th.co.ocac.rcac84.ui.whatsnew.WhatsnewFragment;

import dagger.Component;

/**
 * Created by Ch on 21/10/18.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(LoginActivity activity);

    void inject(SplashActivity activity);

    void inject(EventDetailActivity activity);

    void inject(ArtworkDetailActivity activity);

    void inject(ArtistDetailActivity activity);

    void inject(ArtistsArtworksFragment fragment);

    void inject(ArtistsRelatedFragment fragment);

    void inject(WhatsnewFragment fragment);

    void inject(AllEventsFragment fragment);

    void inject(GalleryFragment fragment);

    void inject(SearchFragment fragment);

    void inject(AboutActivity activity);

    void inject(ArtistProfileFragment fragment);

    void inject(AgendaDialog dialog);

    void inject(NewsActivity activity);

    void inject(EditProfileActivity activity);

    void inject(ProfileActivity activity);

    void inject(RegisterActivity activity);

    void inject(LocationActivity activity);

    void inject(WebviewActivity activity);

}
