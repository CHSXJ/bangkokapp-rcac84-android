package th.co.ocac.rcac84.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.News;
import th.co.ocac.rcac84.di.ActivityContext;
import th.co.ocac.rcac84.di.PerActivity;
import th.co.ocac.rcac84.ui.about.AboutMvpPresenter;
import th.co.ocac.rcac84.ui.about.AboutMvpView;
import th.co.ocac.rcac84.ui.about.AboutPresenter;
import th.co.ocac.rcac84.ui.agendaDialog.AgendaDialogMvpPresenter;
import th.co.ocac.rcac84.ui.agendaDialog.AgendaDialogPresenter;
import th.co.ocac.rcac84.ui.artist.ArtistDetailMvpPresenter;
import th.co.ocac.rcac84.ui.artist.ArtistDetailMvpView;
import th.co.ocac.rcac84.ui.artist.ArtistDetailPagerAdapter;
import th.co.ocac.rcac84.ui.artist.ArtistDetailPresenter;
import th.co.ocac.rcac84.ui.artistsartworks.ArtistsArtworksMvpPresenter;
import th.co.ocac.rcac84.ui.artistsartworks.ArtistsArtworksMvpView;
import th.co.ocac.rcac84.ui.artistsartworks.ArtistsArtworksPresenter;
import th.co.ocac.rcac84.ui.artistsrelated.ArtistsRelatedMvpPresenter;
import th.co.ocac.rcac84.ui.artistsrelated.ArtistsRelatedMvpView;
import th.co.ocac.rcac84.ui.artistsrelated.ArtistsRelatedPresenter;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailMvpPresenter;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailMvpView;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailPresenter;
import th.co.ocac.rcac84.ui.allevents.AllEventsMvpPresenter;
import th.co.ocac.rcac84.ui.allevents.AllEventsMvpView;
import th.co.ocac.rcac84.ui.allevents.AllEventsPresenter;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailMvpPresenter;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailMvpView;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailPresenter;
import th.co.ocac.rcac84.ui.location.LocationMvpPresenter;
import th.co.ocac.rcac84.ui.location.LocationMvpView;
import th.co.ocac.rcac84.ui.location.LocationPresenter;
import th.co.ocac.rcac84.ui.agendaDialog.AgendaDialogMvpView;
import th.co.ocac.rcac84.ui.news.NewsMvpPresenter;
import th.co.ocac.rcac84.ui.news.NewsMvpView;
import th.co.ocac.rcac84.ui.news.NewsPresenter;
import th.co.ocac.rcac84.ui.gallery.GalleryMvpPresenter;
import th.co.ocac.rcac84.ui.gallery.GalleryMvpView;
import th.co.ocac.rcac84.ui.gallery.GalleryPresenter;
import th.co.ocac.rcac84.ui.login.LoginMvpPresenter;
import th.co.ocac.rcac84.ui.login.LoginMvpView;
import th.co.ocac.rcac84.ui.login.LoginPresenter;
import th.co.ocac.rcac84.ui.main.MainMvpPresenter;
import th.co.ocac.rcac84.ui.main.MainMvpView;
import th.co.ocac.rcac84.ui.main.MainPagerAdapter;
import th.co.ocac.rcac84.ui.main.MainPresenter;
import th.co.ocac.rcac84.ui.editprofile.EditProfileMvpPresenter;
import th.co.ocac.rcac84.ui.editprofile.EditProfileMvpView;
import th.co.ocac.rcac84.ui.editprofile.EditProfilePresenter;
import th.co.ocac.rcac84.ui.profile.ProfileMvpPresenter;
import th.co.ocac.rcac84.ui.profile.ProfileMvpView;
import th.co.ocac.rcac84.ui.profile.ProfilePresenter;
import th.co.ocac.rcac84.ui.profile.RegisterMvpPresenter;
import th.co.ocac.rcac84.ui.profile.RegisterMvpView;
import th.co.ocac.rcac84.ui.profile.RegisterPresenter;
import th.co.ocac.rcac84.ui.search.SearchMvpPresenter;
import th.co.ocac.rcac84.ui.search.SearchMvpView;
import th.co.ocac.rcac84.ui.search.SearchPresenter;
import th.co.ocac.rcac84.ui.splash.SplashMvpPresenter;
import th.co.ocac.rcac84.ui.splash.SplashMvpView;
import th.co.ocac.rcac84.ui.splash.SplashPresenter;
import th.co.ocac.rcac84.utils.EventAdapter;
import th.co.ocac.rcac84.utils.NewsAdapter;
import th.co.ocac.rcac84.utils.PreviousEventAdapter;
import th.co.ocac.rcac84.utils.WhatsnewEventAdapter;
import th.co.ocac.rcac84.ui.whatsnew.WhatsnewMvpPresenter;
import th.co.ocac.rcac84.ui.whatsnew.WhatsnewMvpView;
import th.co.ocac.rcac84.ui.whatsnew.WhatsnewPresenter;
import th.co.ocac.rcac84.utils.rx.AppSchedulerProvider;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Ch on 21/10/18.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    AboutMvpPresenter<AboutMvpView> provideAboutPresenter(
            AboutPresenter<AboutMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    AgendaDialogMvpPresenter<AgendaDialogMvpView> provideRateUsPresenter(
            AgendaDialogPresenter<AgendaDialogMvpView> presenter) {
        return presenter;
    }

    @Provides
    NewsMvpPresenter<NewsMvpView> provideNewsPresenter(
            NewsPresenter<NewsMvpView> presenter) {
        return presenter;
    }

    @Provides
    LocationMvpPresenter<LocationMvpView> provideLocationPresenter(
            LocationPresenter<LocationMvpView> presenter) {
        return presenter;
    }

    @Provides
    WhatsnewMvpPresenter<WhatsnewMvpView> provideWhatsnewPresenter(
            WhatsnewPresenter<WhatsnewMvpView> presenter) {
        return presenter;
    }

    @Provides
    AllEventsMvpPresenter<AllEventsMvpView> provideAllEventsPresenter(
            AllEventsPresenter<AllEventsMvpView> presenter) {
        return presenter;
    }

    @Provides
    EventDetailMvpPresenter<EventDetailMvpView> provideEventDetailPresenter(
            EventDetailPresenter<EventDetailMvpView> presenter) {
        return presenter;
    }

    @Provides
    ArtworkDetailMvpPresenter<ArtworkDetailMvpView> provideArtworkDetailPresenter(
            ArtworkDetailPresenter<ArtworkDetailMvpView> presenter) {
        return presenter;
    }

    @Provides
    ArtistDetailMvpPresenter<ArtistDetailMvpView> provideArtistDetailPresenter(
            ArtistDetailPresenter<ArtistDetailMvpView> presenter) {
        return presenter;
    }

    @Provides
    ArtistsRelatedMvpPresenter<ArtistsRelatedMvpView> provideArtistsRelatedPresenter(
            ArtistsRelatedPresenter<ArtistsRelatedMvpView> presenter) {
        return presenter;
    }

    @Provides
    GalleryMvpPresenter<GalleryMvpView> provideGalleryPresenter(
                    GalleryPresenter<GalleryMvpView> presenter) {
        return presenter;
    }

    @Provides
    ArtistsArtworksMvpPresenter<ArtistsArtworksMvpView> provideArtistsArtworksPresenter(
            ArtistsArtworksPresenter<ArtistsArtworksMvpView> presenter) {
        return presenter;
    }

    @Provides
    SearchMvpPresenter<SearchMvpView> provideSearchPresenter(
            SearchPresenter<SearchMvpView> presenter) {
        return presenter;
    }

    @Provides
    EditProfileMvpPresenter<EditProfileMvpView> provideEditProfilePresenter(
            EditProfilePresenter<EditProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    RegisterMvpPresenter<RegisterMvpView> provideRegisterPresenter(
            RegisterPresenter<RegisterMvpView> presenter) {
        return presenter;
    }

    @Provides
    ProfileMvpPresenter<ProfileMvpView> provideProfilePresenter(
            ProfilePresenter<ProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    MainPagerAdapter provideMainPagerAdapter(AppCompatActivity activity) {
        return new MainPagerAdapter(activity.getSupportFragmentManager());
    }

    @Provides
    ArtistDetailPagerAdapter artistDetailPagerAdapter(AppCompatActivity activity) {
        return new ArtistDetailPagerAdapter(activity.getSupportFragmentManager());
    }

    @Provides
    EventAdapter provideEventAdapter() {
        return new EventAdapter(new ArrayList<Event>());
    }

    @Provides
    WhatsnewEventAdapter provideWhatsnewEventAdapter() {
        return new WhatsnewEventAdapter(new ArrayList<Event>());
    }

    @Provides
    PreviousEventAdapter providePreviousEventAdapter() {
        return new PreviousEventAdapter(new ArrayList<Event>());
    }

    @Provides
    NewsAdapter provideNewsAdapter() {
        return new NewsAdapter(new ArrayList<News>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

}
