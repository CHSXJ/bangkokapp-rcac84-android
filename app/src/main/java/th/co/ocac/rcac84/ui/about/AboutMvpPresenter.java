package th.co.ocac.rcac84.ui.about;

import th.co.ocac.rcac84.ui.base.MvpPresenter;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 21/10/18.
 */

public interface AboutMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void loadMuseum(String s);
}
