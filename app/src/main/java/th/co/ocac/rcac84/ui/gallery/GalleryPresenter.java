package th.co.ocac.rcac84.ui.gallery;

import com.androidnetworking.error.ANError;
import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.data.network.model.Tag;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ch on 22/07/18.
 */

public class GalleryPresenter<V extends GalleryMvpView> extends BasePresenter<V> implements
        GalleryMvpPresenter<V> {

    private static final String TAG = "GalleryPresenter";

    @Inject
    public GalleryPresenter(DataManager dataManager,
                            SchedulerProvider schedulerProvider,
                            CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getGalleryByTags(List<String> tagIds) {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getGalleryByTagsApiCall(tagIds)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Artwork>>() {
                    @Override
                    public void accept(List<Artwork> artworks) throws Exception {
                        if (artworks != null) {
                            getMvpView().showGallery(artworks);
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public void getAllTags() {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getAllTagsApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Tag>>() {
                    @Override
                    public void accept(List<Tag> tags) throws Exception {
                        if (tags != null) {
                            getMvpView().showTags(tags);
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

}


