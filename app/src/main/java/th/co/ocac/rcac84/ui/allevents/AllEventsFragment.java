package th.co.ocac.rcac84.ui.allevents;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.EventDate;
import th.co.ocac.rcac84.di.component.ActivityComponent;
import th.co.ocac.rcac84.ui.agendaDialog.AgendaDialog;
import th.co.ocac.rcac84.ui.base.BaseFragment;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailActivity;
import th.co.ocac.rcac84.ui.login.LoginActivity;
import th.co.ocac.rcac84.utils.EventAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllEventsFragment extends BaseFragment implements AllEventsMvpView, EventAdapter.Callback {

    private static final String TAG = "EventFragment";

    @Inject
    AllEventsMvpPresenter<AllEventsMvpView> mPresenter;

    @Inject
    LinearLayoutManager currentLayoutManager;

    @Inject
    EventAdapter mCurrentEventAdapter;

    @BindView(R.id.current_event_recycler_view)
    RecyclerView currentRecyclerView;

    public static AllEventsFragment newInstance() {
        Bundle args = new Bundle();
        AllEventsFragment fragment = new AllEventsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_events, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            mCurrentEventAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

//        currentRecyclerView.setNestedScrollingEnabled(false);
        currentLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        currentRecyclerView.setLayoutManager(currentLayoutManager);
        currentRecyclerView.setItemAnimator(new DefaultItemAnimator());
        currentRecyclerView.setAdapter(mCurrentEventAdapter);

        mPresenter.onCurrentEventViewPrepared();
        mPresenter.onNextEventViewPrepared();

    }

    @Override
    public void onBlogEmptyViewRetryClick() {

    }

    @Override
    public void updateCurrentEvent(List<Event> eventList) {
        Event h = new Event();
        h.setHeader(true);
        h.setName(getString(R.string.current_events));
        mCurrentEventAdapter.addItem(h);
        mCurrentEventAdapter.addItems(eventList);
    }

    @Override
    public void updateNextEvent(List<Event> eventList) {
        Event h = new Event();
        h.setHeader(true);
        h.setName(getString(R.string.next_events));
        mCurrentEventAdapter.addItem(h);
        mCurrentEventAdapter.addItems(eventList);
    }

    @Override
    public void openEventDetail(String eventId) {
        Log.d(TAG, "openEventDetail " + eventId);
        startActivity(EventDetailActivity.getStartIntent(getActivity(), eventId));
    }

    @Override
    public void onJoinEvent(String eventId) {

        Log.d(TAG, "onJoinEvent " + eventId);
        if (mPresenter.getUserToken() != null) {
            mPresenter.joinEvent(eventId);
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle("Before joining this event, please sign in first");
            alert.setPositiveButton("Sign in", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(LoginActivity.getStartIntent(getContext()));
                }
            });
            alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alert.show();
        }
    }

    @Override
    public void onDisplayAgenda(List<EventDate> dateList) {
        AgendaDialog.newInstance(dateList).show(getActivity().getSupportFragmentManager());
//        scrollView.smoothScrollTo(0,0);
    }

    @Override
    public void showJoinAlert(final String eventId, String msg) {
        hideLoading();

        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle(msg);
        alert.setPositiveButton("See Event", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                openEventDetail(eventId);
            }
        });
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alert.show();
    }


}
