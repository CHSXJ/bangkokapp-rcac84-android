package th.co.ocac.rcac84.ui.artistsartworks;

import th.co.ocac.rcac84.ui.base.MvpPresenter;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 22/07/18.
 */

public interface ArtistsArtworksMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    void getArtworksByArtistId(String artistId);
}
