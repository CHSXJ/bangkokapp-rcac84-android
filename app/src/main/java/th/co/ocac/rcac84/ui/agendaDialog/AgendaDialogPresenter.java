package th.co.ocac.rcac84.ui.agendaDialog;


import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Ch on 23/10/18.
 */

public class AgendaDialogPresenter<V extends AgendaDialogMvpView> extends BasePresenter<V>
        implements AgendaDialogMvpPresenter<V> {

    public static final String TAG = "AgendaDialogPresenter";

    @Inject
    public AgendaDialogPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onCloseClicked() {
        getMvpView().dismissDialog();
    }

}
