package th.co.ocac.rcac84.ui.editprofile;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.ui.base.BaseActivity;
import th.co.ocac.rcac84.ui.main.MainActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ch on 12/10/18.
 */

public class EditProfileActivity extends BaseActivity implements EditProfileMvpView {

    public static String TAG = "ProfileActivity";

    @Inject
    EditProfileMvpPresenter<EditProfileMvpView> mPresenter;

    @BindView(R.id.email)
    EditText mEmailEditText;

    @BindView(R.id.title)
    Button mTitleEditText;

    @BindView(R.id.bday)
    Button mBDayEditText;

    @BindView(R.id.firstname)
    EditText mFirstnameEditText;

    @BindView(R.id.lastname)
    EditText mLastnameEditText;

    @BindView(R.id.nationality)
    Button mNationalityEditText;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

//    @BindView(R.id.login_button)
//    LoginButton loginButton;



    private LoginButton txtFbLogin;
    private AccessToken mAccessToken;
    private CallbackManager callbackManager;
    private User mCurrentUser;
    private Calendar calendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    CountryPicker.Builder builder =
            new CountryPicker.Builder().with(this)
                    .listener(new OnCountryPickerListener() {
                        @Override
                        public void onSelectCountry(Country country) {
                            mNationalityEditText.setText(country.getName());
                            mNationalityEditText.setTag(country.getCode());
                        }
                    });

    CountryPicker picker = builder.build();

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(EditProfileActivity.this);

        setUp();

    }

    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.getStartIntent(EditProfileActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void displayUser(User user) {

        Log.d(TAG, "Logged in user id: " + user.getId());
        mCurrentUser = user;
        mEmailEditText.setText(user.getEmail());
        mTitleEditText.setText(user.getTitle());
        mFirstnameEditText.setText(user.getFirstName());
        mLastnameEditText.setText(user.getLastName());
        mBDayEditText.setText(user.getBirthDate());
        mNationalityEditText.setText(picker.getCountryByISO(user.getNationality()).getName());

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {

        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        callbackManager = CallbackManager.Factory.create();
        mPresenter.getLoggedInProfile();

    }

    private void updateLabel() {
        String myFormat = "mm/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        mBDayEditText.setText(sdf.format(calendar.getTime()));
    }

    @OnClick(R.id.bday)
    public void pressBDay(){
        new DatePickerDialog(this, date, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.title)
    public void pressTitle(){

        final CharSequence[] items = {
                "Mr.", "Ms.", "Mrs."
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Make your selection");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                mTitleEditText.setText(items[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @OnClick(R.id.nationality)
    public void pressNationality(){
        picker.showDialog(getSupportFragmentManager());
    }

    @OnClick(R.id.btn_save)
    public void saveProfile() {

        User newProfile = mCurrentUser;
        newProfile.setEmail(mEmailEditText.getText().toString());
        newProfile.setLogin(mEmailEditText.getText().toString());
        newProfile.setFirstName(mFirstnameEditText.getText().toString());
        newProfile.setLastName(mLastnameEditText.getText().toString());
        newProfile.setBirthDate(mBDayEditText.getText().toString());
        newProfile.setNationality(mNationalityEditText.getTag().toString());
        newProfile.setTitle(mTitleEditText.getText().toString());
        mPresenter.saveProfile(newProfile);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {

            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (upIntent == null) finish();
                else {
                    upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                        // This activity is NOT part of this app's task, so create a new task
                        // when navigating up, with a synthesized back stack.
                        TaskStackBuilder.create(this)
                                // Add all of this activity's parents to the back stack
                                .addNextIntentWithParentStack(upIntent)
                                // Navigate up to the closest parent
                                .startActivities();
                    } else {
                        // This activity is part of this app's task, so simply
                        // navigate up to the logical parent activity.
                        NavUtils.navigateUpTo(this, upIntent);
                    }
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode,  data);
    }

}
