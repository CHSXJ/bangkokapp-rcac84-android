package th.co.ocac.rcac84.ui.location;

import th.co.ocac.rcac84.ui.base.MvpPresenter;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 21/10/18.
 */

public interface LocationMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void loadMuseum(String s);
}
