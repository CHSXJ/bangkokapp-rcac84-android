package th.co.ocac.rcac84.ui.artworkdetail;

import com.androidnetworking.error.ANError;
import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ch on 22/07/18.
 */

public class ArtworkDetailPresenter<V extends ArtworkDetailMvpView> extends BasePresenter<V> implements
        ArtworkDetailMvpPresenter<V> {

    private static final String TAG = "ArtworkDetailPresenter";

    @Inject
    public ArtworkDetailPresenter(DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onEventDetailPrepared(String mArtworkId) {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getArtworkDetail(mArtworkId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<Artwork>() {
                    @Override
                    public void accept(Artwork artwork) throws Exception {
                        if (artwork != null) {
                            getMvpView().updateArtworkDetailView(artwork);
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}


