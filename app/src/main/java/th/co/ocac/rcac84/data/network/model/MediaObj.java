package th.co.ocac.rcac84.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MediaObj {

    public enum MediaType {
        IMAGE, VIDEO, YOUTUBE
    }

    @Expose
    @SerializedName("type")
    private MediaType type;

    @Expose
    @SerializedName("contentType")
    private String contentType;

    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("thumbUrl")
    private String thumbUrl;

    public MediaType getType() {
        return type;
    }

    public void setType(MediaType type) {
        this.type = type;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }
}
