package th.co.ocac.rcac84.ui.gallery;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.data.network.model.Tag;
import th.co.ocac.rcac84.di.component.ActivityComponent;
import th.co.ocac.rcac84.ui.artwork.ArtworkAdapter;
import th.co.ocac.rcac84.ui.artwork.GalleryGridView;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailActivity;
import th.co.ocac.rcac84.ui.base.BaseFragment;
import th.co.ocac.rcac84.utils.EndlessScrollListener;
import com.cunoraz.tagview.TagView;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class GalleryFragment extends BaseFragment implements GalleryMvpView {

    private static final String TAG = "GalleryFragment";

    @Inject
    GalleryMvpPresenter<GalleryMvpView> mPresenter;

    List<String> selectedTagIds;
    List<Tag> mTags;
    ArtworkAdapter mArtworkAdapter;

    @BindView(R.id.gridview)
    GalleryGridView galleryGridView;

    @BindView(R.id.tagsView)
    TagView tagsView;

    List<Artwork> allArtwork;
    List<Artwork> displayedArtwork;

    public static GalleryFragment newInstance() {
        Bundle args = new Bundle();
        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        mArtworkAdapter = new ArtworkAdapter(this.getContext());
        galleryGridView.setAdapter(mArtworkAdapter);

        galleryGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                startActivity(ArtworkDetailActivity.getStartIntent(getActivity(), mArtworkAdapter.getItemAtIndex(position).getId()));
            }
        });

        galleryGridView.setOnScrollListener(new EndlessScrollListener(15) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                loadNextData(page);
                // or loadNextDataFromApi(totalItemsCount); 
                return true; // ONLY if more data is actually being loaded; false otherwise.
            }
        });

        selectedTagIds = new ArrayList<>();

        mPresenter.getGalleryByTags(selectedTagIds);
        mPresenter.getAllTags();
    }

    private void loadNextData(int page) {
        Timber.d("loadNextData" + displayedArtwork.size() +" "+allArtwork.size() );
        if (displayedArtwork.size() < allArtwork.size()) {
            mArtworkAdapter.removeALlItems();
            int n = displayedArtwork.size();
            if (n+15 > allArtwork.size()) {
                n = allArtwork.size();
            } else {
                n += 15;
            }
            for (int i = displayedArtwork.size(); i < n; i++) {
                displayedArtwork.add(allArtwork.get(i));
            }
            mArtworkAdapter.addItems(displayedArtwork);
        }
    }

    @Override
    public void showGallery(List<Artwork> artworks) {
//        mArtworkAdapter.addItems(artworks);
        allArtwork = artworks;
        displayedArtwork = new ArrayList<>();
        loadNextData(1);
    }

    @Override
    public void showTags(List<Tag> tags) {

        mTags = tags;

        for (final Tag tag: tags) {
            Log.d(TAG, tag.getId()+" "+tag.getName());
            com.cunoraz.tagview.Tag t = new com.cunoraz.tagview.Tag(tag.getName());
            t.layoutColor = ContextCompat.getColor(getContext(),R.color.colorPrimaryTrans);
            t.layoutColorPress = ContextCompat.getColor(getContext(),R.color.colorAccent);
            t.radius = 15;

            tagsView.addTag(t);
        }

        tagsView.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(com.cunoraz.tagview.Tag tag, int i) {
                if (tag.id == 0) {//set selected
                    tag.id = 1;
                    tag.layoutColor = ContextCompat.getColor(getContext(), R.color.colorAccent);
                    tag.tagTextColor = Color.BLACK;
                    tag.layoutColorPress = ContextCompat.getColor(getContext(), R.color.colorPrimaryTrans);
                    selectedTagIds.add(mTags.get(i).getId());
                } else { //set unselected
                    tag.id = 0;
                    tag.layoutColorPress = ContextCompat.getColor(getContext(), R.color.colorAccent);
                    tag.tagTextColor = Color.WHITE;
                    tag.layoutColor = ContextCompat.getColor(getContext(), R.color.colorPrimaryTrans);
                    selectedTagIds.remove(mTags.get(i).getId());
                }
                //for refresh view
                tagsView.addTag(new com.cunoraz.tagview.Tag(""));
                tagsView.remove(mTags.size());
                //
                Log.d(TAG, selectedTagIds.toString());
                mPresenter.getGalleryByTags(selectedTagIds);

            }
        });

    }
}
