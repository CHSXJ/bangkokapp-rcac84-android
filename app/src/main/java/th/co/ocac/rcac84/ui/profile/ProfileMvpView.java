
package th.co.ocac.rcac84.ui.profile;

import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.ui.base.MvpView;

import java.util.List;

/**
 * Created by Ch on 16/10/18.
 */

public interface ProfileMvpView extends MvpView {

    void openMainActivity();

    void displayUser(User user);

    void displayPreviousEvent(List<Event> events);

    void signedOut();
}
