
package th.co.ocac.rcac84.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ch on 12/09/18.
 */

public class Event {

    @Expose
    @SerializedName("header")
    private Boolean isHeader = false;

    public Boolean getHeader() {
        return isHeader;
    }

    public void setHeader(Boolean header) {
        isHeader = header;
    }

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("museumId")
    private String museumId;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("about")
    private String about;

    @Expose
    @SerializedName("dateList")
    private List<EventDate> dateList;

    @Expose
    @SerializedName("dateString")
    private String dateString;

    @Expose
    @SerializedName("location")
    private String location;

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("mediaList")
    private List<MediaObj> mediaList;

    @Expose
    @SerializedName("tags")
    private List<Tag> tags;

    @Expose
    @SerializedName("sampleArtworks")
    private List<Sample> sampleArtworks;

    @Expose
    @SerializedName("sampleArtists")
    private List<Sample> sampleArtists;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMuseumId() {
        return museumId;
    }

    public void setMuseumId(String museumId) {
        this.museumId = museumId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public List<EventDate> getDateList() {
        return dateList;
    }

    public void setDateList(List<EventDate> dateList) {
        this.dateList = dateList;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MediaObj> getMediaList() {
        return mediaList;
    }

    public void setMediaList(List<MediaObj> mediaList) {
        this.mediaList = mediaList;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Sample> getSampleArtworks() {
        return sampleArtworks;
    }

    public void setSampleArtworks(List<Sample> sampleArtworks) {
        this.sampleArtworks = sampleArtworks;
    }

    public List<Sample> getSampleArtists() {
        return sampleArtists;
    }

    public void setSampleArtists(List<Sample> sampleArtists) {
        this.sampleArtists = sampleArtists;
    }
}
