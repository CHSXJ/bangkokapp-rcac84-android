package th.co.ocac.rcac84.ui.allevents;

import th.co.ocac.rcac84.ui.base.MvpPresenter;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 22/07/18.
 */

public interface AllEventsMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    void onCurrentEventViewPrepared();

    void onNextEventViewPrepared();

    String getUserToken();

    void joinEvent(String eventId);
}
