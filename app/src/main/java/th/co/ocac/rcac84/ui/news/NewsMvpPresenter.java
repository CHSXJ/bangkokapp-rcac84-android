package th.co.ocac.rcac84.ui.news;

import th.co.ocac.rcac84.ui.base.MvpPresenter;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 21/10/18.
 */

public interface NewsMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    void loadNews();

}
