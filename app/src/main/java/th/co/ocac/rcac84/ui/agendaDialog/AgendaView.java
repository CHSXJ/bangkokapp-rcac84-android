package th.co.ocac.rcac84.ui.agendaDialog;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Agenda;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AgendaView extends LinearLayout {

    @BindView(R.id.title_text_view)
    TextView titleTextView;

    @BindView(R.id.date_text_view)
    TextView dateTextView;

    @BindView(R.id.content_text_view)
    TextView contentTextView;

    public AgendaView(Context context) {
        super(context);
        setup(context);
    }

    public AgendaView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setup(context);
    }

    public AgendaView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AgendaView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setup(context);
    }

    private void setup(Context mContext) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_previous_event_view, this, true);
        ButterKnife.bind(this, view);
    }

    public void addAganda(Agenda agenda) {
        titleTextView.setText(agenda.getName());
        contentTextView.setText(agenda.getDescription());
        dateTextView.setText(agenda.getTime());
    }
}
