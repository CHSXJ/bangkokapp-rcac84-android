
package th.co.ocac.rcac84.ui.editprofile;

import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.ui.base.MvpView;

/**
 * Created by Ch on 25/10/18.
 */

public interface EditProfileMvpView extends MvpView {

    void openMainActivity();

    void displayUser(User user);
}
