package th.co.ocac.rcac84.ui.artistsartworks;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.di.component.ActivityComponent;
import th.co.ocac.rcac84.ui.artwork.ArtworkAdapter;
import th.co.ocac.rcac84.ui.artwork.GalleryGridView;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailActivity;
import th.co.ocac.rcac84.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArtistsArtworksFragment extends BaseFragment implements ArtistsArtworksMvpView {

    private static final String TAG = "ArtistsRelatedFragment";

    @Inject
    ArtistsArtworksMvpPresenter<ArtistsArtworksMvpView> mPresenter;

    static String mArtistId;
    ArtworkAdapter mArtworkAdapter;

    @BindView(R.id.gridview)
    GalleryGridView galleryGridView;

    public static ArtistsArtworksFragment newInstance(String artistId) {
        Bundle args = new Bundle();
        ArtistsArtworksFragment fragment = new ArtistsArtworksFragment();
        fragment.setArguments(args);
        mArtistId = artistId;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void setUp(View view) {

        mArtworkAdapter = new ArtworkAdapter(this.getContext());
        galleryGridView.setAdapter(mArtworkAdapter);

        galleryGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                startActivity(ArtworkDetailActivity.getStartIntent(getActivity(), mArtworkAdapter.getItemAtIndex(position).getId()));
            }
        });

        mPresenter.getArtworksByArtistId(mArtistId);
    }

    @Override
    public void showGallery(List<Artwork> artworks) {
        mArtworkAdapter.addItems(artworks);
    }
}
