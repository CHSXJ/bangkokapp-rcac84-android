package th.co.ocac.rcac84.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Artist {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("awards")
    private String awards;

    @Expose
    @SerializedName("education")
    private String education;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("mobile")
    private String mobile;

    @Expose
    @SerializedName("experience")
    private String experience;

    @Expose
    @SerializedName("nameen")
    private String nameen;

    @Expose
    @SerializedName("nameth")
    private String nameth;

    @Expose
    @SerializedName("nationality")
    private String nationality;

    @Expose
    @SerializedName("profilePic")
    private String profilePic;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getNameen() {
        return nameen;
    }

    public void setNameen(String nameen) {
        this.nameen = nameen;
    }

    public String getNameth() {
        return nameth;
    }

    public void setNameth(String nameth) {
        this.nameth = nameth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    @Expose
    @SerializedName("story")
    private String story;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
