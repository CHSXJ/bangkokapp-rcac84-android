package th.co.ocac.rcac84.ui.agendaDialog;

import th.co.ocac.rcac84.ui.base.DialogMvpView;

/**
 * Created by Ch on 23/10/18.
 */

public interface AgendaDialogMvpView extends DialogMvpView {

    void dismissDialog();
}
