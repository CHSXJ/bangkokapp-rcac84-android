package th.co.ocac.rcac84.ui.artist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Artist;
import th.co.ocac.rcac84.di.component.ActivityComponent;
import th.co.ocac.rcac84.ui.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArtistProfileFragment extends BaseFragment implements ArtistProfileMvpView{

    private static final String TAG = "ArtistProfileFragment";

    static Artist mArtist;

    @BindView(R.id.awards)
    TextView awards;

    @BindView(R.id.experience)
    TextView experience;

    @BindView(R.id.education)
    TextView education;

    @BindView(R.id.story)
    TextView story;

    @BindView(R.id.contact)
    TextView contact;

    public static ArtistProfileFragment newInstance(Artist artist) {
        Bundle args = new Bundle();
        ArtistProfileFragment fragment = new ArtistProfileFragment();
        fragment.setArguments(args);
        mArtist = artist;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artist_profile, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
//            mPresenter.onAttach(this);
//            mEventAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        awards.setText(mArtist.getAwards());
        experience.setText(mArtist.getExperience());
        education.setText(mArtist.getEducation());
        story.setText(mArtist.getStory());
        contact.setText("Email : " + mArtist.getEmail() + "\nTel. : " + mArtist.getMobile());

    }

}
