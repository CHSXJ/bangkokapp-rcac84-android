package th.co.ocac.rcac84.ui.whatsnew;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.EventDate;
import th.co.ocac.rcac84.di.component.ActivityComponent;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailActivity;
import th.co.ocac.rcac84.ui.artwork.ArtworkAdapter;
import th.co.ocac.rcac84.ui.base.BaseFragment;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailActivity;
import th.co.ocac.rcac84.ui.login.LoginActivity;
import th.co.ocac.rcac84.ui.agendaDialog.AgendaDialog;
import th.co.ocac.rcac84.utils.WhatsnewEventAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class WhatsnewFragment extends BaseFragment implements WhatsnewMvpView, WhatsnewEventAdapter.Callback {

    private static final String TAG = "WhatsnewFragment";

    @Inject
    WhatsnewMvpPresenter<WhatsnewMvpView> mPresenter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    WhatsnewEventAdapter mEventAdapter;

    ArtworkAdapter mArtworkAdapter;

    @BindView(R.id.events_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.gridview)
    GridView artOfTheMonth;

    List<Artwork> allArtOfMonth;
    List<Artwork> displayedArtOfMonth;

    public static WhatsnewFragment newInstance() {
        Bundle args = new Bundle();
        WhatsnewFragment fragment = new WhatsnewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_whatsnew, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            mEventAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mEventAdapter);

        mArtworkAdapter = new ArtworkAdapter(this.getContext());
        artOfTheMonth.setAdapter(mArtworkAdapter);

        artOfTheMonth.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                startActivity(ArtworkDetailActivity.getStartIntent(getActivity(), mArtworkAdapter.getItemAtIndex(position).getId()));

            }
        });

        mPresenter.loadCurrentEvents();
        mPresenter.loadArtOfTheMonth();

    }

    @Override
    public void onBlogEmptyViewRetryClick() {

    }

    @Override
    public void openEventDetail(String eventId) {
        Log.d(TAG, "openEventDetail " + eventId);
        startActivity(EventDetailActivity.getStartIntent(getActivity(), eventId));
    }

    @Override
    public void onJoinEvent(String eventId) {

        Log.d(TAG, "onJoinEvent " + eventId);
        if (mPresenter.getUserToken() != null) {
            mPresenter.joinEvent(eventId);
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle("Before joining this event, please sign in first");
            alert.setPositiveButton("Sign in", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(LoginActivity.getStartIntent(getContext()));
                }
            });
            alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alert.show();
        }
    }

    @Override
    public void onDisplayAgenda(List<EventDate> dateList) {
        AgendaDialog.newInstance(dateList).show(getActivity().getSupportFragmentManager());
    }

    @Override
    public void showJoinAlert(final String eventId, String msg) {
        hideLoading();

        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle(msg);
        alert.setPositiveButton("See Event", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                openEventDetail(eventId);
            }
        });
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alert.show();
    }

    @Override
    public void updateEvent(List<Event> eventList) {
        mEventAdapter.addItems(eventList);
    }

    @Override
    public void updateArtworks(List<Artwork> artworks) {
        allArtOfMonth = artworks;
        displayedArtOfMonth = new ArrayList<>();
        loadNextData(1);
    }

    private void loadNextData(int page) {
        Timber.d("loadNextData" + displayedArtOfMonth.size() +" "+allArtOfMonth.size() );
        if (displayedArtOfMonth.size() < allArtOfMonth.size()) {
            mArtworkAdapter.removeALlItems();
            int n = displayedArtOfMonth.size();
            if (n+9 > allArtOfMonth.size()) {
                n = allArtOfMonth.size();
            } else {
                n += 9;
            }
            for (int i = displayedArtOfMonth.size(); i < n; i++) {
                displayedArtOfMonth.add(allArtOfMonth.get(i));
            }
            mArtworkAdapter.addItems(displayedArtOfMonth);
        }
    }

}
