package th.co.ocac.rcac84.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.SearchResponse;
import th.co.ocac.rcac84.di.component.ActivityComponent;
import th.co.ocac.rcac84.ui.artist.ArtistAdapter;
import th.co.ocac.rcac84.ui.artist.ArtistDetailActivity;
import th.co.ocac.rcac84.ui.artwork.ArtworkAdapter;
import th.co.ocac.rcac84.ui.artwork.GalleryGridView;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailActivity;
import th.co.ocac.rcac84.ui.base.BaseFragment;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailActivity;
import th.co.ocac.rcac84.utils.PreviousEventAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchFragment extends BaseFragment implements SearchMvpView, PreviousEventAdapter.Callback {

    private static final String TAG = "SearchFragment";

    @Inject
    SearchMvpPresenter<SearchMvpView> mPresenter;

    @BindView(R.id.input)
    EditText input;

    @BindView(R.id.artworksGridview)
    GalleryGridView artworksGridview;

    @BindView(R.id.artistsGridview)
    GalleryGridView artistsGridview;

    @BindView(R.id.eventsRecyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.artworksView)
    LinearLayout artworksView;

    @BindView(R.id.artistView)
    LinearLayout artistView;

    @BindView(R.id.eventsView)
    LinearLayout eventsView;

    ArtworkAdapter mArtworkAdapter;
    ArtistAdapter mArtistAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    PreviousEventAdapter eventAdapter;

    public static SearchFragment newInstance() {
        Bundle args = new Bundle();
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            eventAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        mPresenter.onViewPrepared();
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPresenter.searching(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mArtworkAdapter = new ArtworkAdapter(this.getContext());
        artworksGridview.setAdapter(mArtworkAdapter);

        artworksGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                startActivity(ArtworkDetailActivity.getStartIntent(getActivity(), mArtworkAdapter.getItemAtIndex(position).getId()));
            }
        });

        mArtistAdapter = new ArtistAdapter(this.getContext());
        artistsGridview.setAdapter(mArtistAdapter);

        artistsGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                startActivity(ArtistDetailActivity.getStartIntent(getActivity(), mArtistAdapter.getItemAtIndex(position).getId()));
            }
        });

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(eventAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        displayEmpty();

    }

    @Override
    public void displaySearchResult(SearchResponse searchResponse) {
        hideLoading();
        Log.d(TAG, String.valueOf(searchResponse));
        if (searchResponse.getArtworks().size() == 0) {
            artworksView.setVisibility(View.GONE);
        } else {
            artworksView.setVisibility(View.VISIBLE);
            mArtworkAdapter.addItems(searchResponse.getArtworks());
        }

        if (searchResponse.getArtists().size() == 0) {
            artistView.setVisibility(View.GONE);
        } else {
            artistView.setVisibility(View.VISIBLE);
            mArtistAdapter.addItems(searchResponse.getArtists());
        }

        if (searchResponse.getEvents().size() == 0) {
            eventsView.setVisibility(View.GONE);
        } else {
            eventsView.setVisibility(View.VISIBLE);
            eventAdapter.addItems(searchResponse.getEvents());
        }
    }

    @Override
    public void displayEmpty() {
        hideLoading();
        artworksView.setVisibility(View.GONE);
        artistView.setVisibility(View.GONE);
        eventsView.setVisibility(View.GONE);
    }

    @Override
    public void onEventClick(String id) {
        Log.d(TAG, "openEventDetail " + id);
        startActivity(EventDetailActivity.getStartIntent(getActivity(), id));
    }
}
