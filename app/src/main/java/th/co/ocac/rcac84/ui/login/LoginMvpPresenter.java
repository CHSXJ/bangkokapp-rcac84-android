package th.co.ocac.rcac84.ui.login;


import th.co.ocac.rcac84.di.PerActivity;
import th.co.ocac.rcac84.ui.base.MvpPresenter;

/**
 * Created by janisharali on 27/01/17.
 */

@PerActivity
public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {

    void onServerLoginClick(String email, String password);

    void onFacebookLoginClick(String fbId, String fbToken);

}
