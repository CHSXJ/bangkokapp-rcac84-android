
package th.co.ocac.rcac84.ui.agendaDialog;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Agenda;
import th.co.ocac.rcac84.data.network.model.EventDate;
import th.co.ocac.rcac84.di.component.ActivityComponent;
import th.co.ocac.rcac84.ui.base.BaseDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ch on 23/10/18.
 */

public class AgendaDialog extends BaseDialog implements AgendaDialogMvpView {

    private static final String TAG = "AgendaDialog";

    @Inject
    AgendaDialogMvpPresenter<AgendaDialogMvpView> mPresenter;

    @BindView(R.id.inputView)
    LinearLayout inputView;

    static List<EventDate> mEventDate;


    public static AgendaDialog newInstance(List<EventDate> dateList) {

        mEventDate = dateList;

        AgendaDialog fragment = new AgendaDialog();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_agenda, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {

            component.inject(this);

            setUnBinder(ButterKnife.bind(this, view));

            mPresenter.onAttach(this);
        }

        return view;
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }


    @OnClick(R.id.btn_close)
    void onLaterClick() {
        mPresenter.onCloseClicked();
    }


    @Override
    public void dismissDialog() {
        super.dismissDialog(TAG);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    protected void setUp(View view) {

        for (EventDate date: mEventDate) {

            HeaderView headerView = new HeaderView(getContext());
            headerView.addAganda(date.getDateStr());
            inputView.addView(headerView);

            if (date.getAgendas().size() == 0) {
                AgendaView agendaView = new AgendaView(getContext());
                Agenda agenda = new Agenda();
                agenda.setName("No schedule for today.");
                agendaView.addAganda(agenda);
                inputView.addView(agendaView);
            } else {
                for (Agenda agenda : date.getAgendas()) {
                    AgendaView agendaView = new AgendaView(getContext());
                    agendaView.addAganda(agenda);
                    inputView.addView(agendaView);
                }
            }
        }
    }
}