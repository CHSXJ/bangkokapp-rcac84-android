package th.co.ocac.rcac84.ui.news;

import th.co.ocac.rcac84.data.network.model.News;
import th.co.ocac.rcac84.ui.base.MvpView;

import java.util.List;

/**
 * Created by Ch on 21/10/18.
 */

public interface NewsMvpView extends MvpView {

    void displayNews(List<News> news);
}
