package th.co.ocac.rcac84.ui.eventdetail;

import com.androidnetworking.error.ANError;
import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ch on 22/07/18.
 */

public class EventDetailPresenter<V extends EventDetailMvpView> extends BasePresenter<V> implements
        EventDetailMvpPresenter<V> {

    private static final String TAG = "EventDetailPresenter";

    @Inject
    public EventDetailPresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onEventDetailPrepared(String mEventId) {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getEventDetail(mEventId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<Event>() {
                    @Override
                    public void accept(@NonNull Event eventResponse)
                            throws Exception {
                        if (eventResponse != null) {
                            getMvpView().updateEventDetailView(eventResponse);
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}


