package th.co.ocac.rcac84.ui.artworkdetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.data.network.model.Tag;
import th.co.ocac.rcac84.ui.artist.ArtistDetailActivity;
import th.co.ocac.rcac84.ui.base.BaseActivity;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailActivity;
import th.co.ocac.rcac84.utils.PreviousEventAdapter;
import th.co.ocac.rcac84.utils.RecycleItemClickListener;
import com.bumptech.glide.Glide;
import com.cunoraz.tagview.TagView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArtworkDetailActivity extends BaseActivity implements ArtworkDetailMvpView, PreviousEventAdapter.Callback {

    private static final String TAG = "ArtworkDetailActivity";

    @Inject
    ArtworkDetailMvpPresenter<ArtworkDetailMvpView> mPresenter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    PreviousEventAdapter mEventAdapter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.artworkName)
    TextView artworkName;

    @BindView(R.id.artistName)
    TextView artistName;

    @BindView(R.id.detail)
    TextView detail;

    @BindView(R.id.technique)
    TextView technique;

    @BindView(R.id.size)
    TextView size;

    @BindView(R.id.profileImage)
    ImageView profileImage;

    @BindView(R.id.story)
    TextView story;

    @BindView(R.id.previous_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.tagListView)
    TagView tagListView;

    String mArtworkId;

    @BindView(R.id.scrollView)
    ScrollView scrollView;
    private String artistId;

    public static Intent getStartIntent(Context context, String artworkId) {
        Intent intent = new Intent(context, ArtworkDetailActivity.class);
        intent.putExtra("artworkId", artworkId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artwork_detail);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void setUp() {

        setSupportActionBar(mToolbar);

        Intent intent = getIntent();
        mArtworkId = intent.getStringExtra("artworkId");
        mPresenter.onEventDetailPrepared(mArtworkId);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mLayoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mEventAdapter);
        mRecyclerView.addOnItemTouchListener(new RecycleItemClickListener(this, mRecyclerView, new RecycleItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d(TAG, "onClickEvent " + position);
                startActivity(EventDetailActivity.getStartIntent(getApplicationContext(), mEventAdapter.getItemAtIndex(position).getId()));
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (upIntent == null) finish();
                else {
                    upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                        // This activity is NOT part of this app's task, so create a new task
                        // when navigating up, with a synthesized back stack.
                        TaskStackBuilder.create(this)
                                // Add all of this activity's parents to the back stack
                                .addNextIntentWithParentStack(upIntent)
                                // Navigate up to the closest parent
                                .startActivities();
                    } else {
                        // This activity is part of this app's task, so simply
                        // navigate up to the logical parent activity.
                        NavUtils.navigateUpTo(this, upIntent);
                    }
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void updateArtworkDetailView(Artwork artwork) {
        artworkName.setText(artwork.getName());
        artistName.setText(artwork.getArtist().getNameen());
        Glide.with(this).load(artwork.getImage()).into(imageView);
        technique.setText("เทคนิค : \n" + artwork.getTechnique());
        size.setText("ขนาด : \n" + artwork.getSize());
        detail.setText("แนวความคิด : \n" + artwork.getConcept());

        Glide.with(this).load(artwork.getArtist().getProfilePic()).into(profileImage);
        story.setText(artwork.getArtist().getStory());

        mEventAdapter.addItems(artwork.getEvents());
        this.artistId = artwork.getArtist().getId();

        for (final Tag tag : artwork.getTags()) {
            com.cunoraz.tagview.Tag t = new com.cunoraz.tagview.Tag(tag.getName());
            t.layoutColor = ContextCompat.getColor(this, R.color.colorAccent);
            t.tagTextColor = Color.BLACK;
            t.radius = 15;

            tagListView.addTag(t);
        }
    }

    @Override
    public void onEventClick(String id) {

    }

    @OnClick(R.id.btn_artist)
    public void goToArtistDetail() {
        startActivity(ArtistDetailActivity.getStartIntent(this, artistId));
//        startActivity(ArtworkDetailActivity.getStartIntent(getActivity(), mArtworkAdapter.getItemAtIndex(position).getId()));
    }
}