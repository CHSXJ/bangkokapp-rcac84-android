package th.co.ocac.rcac84.ui.agendaDialog;

import th.co.ocac.rcac84.ui.base.MvpPresenter;

/**
 * Created by Ch on 23/10/18.
 */

public interface AgendaDialogMvpPresenter<V extends AgendaDialogMvpView> extends MvpPresenter<V> {

    void onCloseClicked();

}
