package th.co.ocac.rcac84.ui.artist;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.data.network.model.Artist;
import th.co.ocac.rcac84.ui.base.BaseActivity;
import th.co.ocac.rcac84.ui.custom.RoundedImageView;
import th.co.ocac.rcac84.utils.DecoderActivity;
import com.bumptech.glide.Glide;
import com.jwang123.flagkit.FlagKit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ch on 25/10/18.
 */

public class ArtistDetailActivity extends BaseActivity implements ArtistDetailMvpView {

    @Inject
    ArtistDetailMvpPresenter<ArtistDetailMvpView> mPresenter;

    @Inject
    ArtistDetailPagerAdapter mPagerAdapter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;

    @BindView(R.id.feed_view_pager)
    ViewPager mViewPager;

    String mArtistId;

    @BindView(R.id.artistName)
    TextView atristName;

    @BindView(R.id.age)
    TextView age;

    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.flag)
    ImageView flag;

    private TextView mNameTextView;

    private TextView mEmailTextView;

    private RoundedImageView mProfileImageView;

//    private ActionBarDrawerToggle mDrawerToggle;

    public static Intent getStartIntent(Context context, String artistId) {
        Intent intent = new Intent(context, ArtistDetailActivity.class);
        intent.putExtra("artistId", artistId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        setUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (mDrawer != null)
//            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onFragmentAttached() {
    }

    @Override
    public void onFragmentDetached(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                    .remove(fragment)
                    .commitNow();
        }
    }

    @Override
    public void updateArtistView(Artist artist) {
        //set pager adapter
        mPagerAdapter.setCount(3);
        mPagerAdapter.setmArtist(artist);
        mViewPager.setAdapter(mPagerAdapter);

        atristName.setText(artist.getNameen());
        try {
            flag.setImageDrawable(FlagKit.drawableWithFlag(this, artist.getNationality().toLowerCase()));
        } catch (Exception e) {
            flag.setVisibility(View.GONE);
        }
//        age.setText("- years old");
        Glide.with(this).load(artist.getProfilePic()).into(imageView);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {

            case R.id.action_scan:
                Intent intent = new Intent(getApplicationContext(), DecoderActivity.class);
                startActivity(intent);
                return true;
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (upIntent == null) finish();
                else {
                    upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                        // This activity is NOT part of this app's task, so create a new task
                        // when navigating up, with a synthesized back stack.
                        TaskStackBuilder.create(this)
                                // Add all of this activity's parents to the back stack
                                .addNextIntentWithParentStack(upIntent)
                                // Navigate up to the closest parent
                                .startActivities();
                    } else {
                        // This activity is part of this app's task, so simply
                        // navigate up to the logical parent activity.
                        NavUtils.navigateUpTo(this, upIntent);
                    }
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void setUp() {

        setSupportActionBar(mToolbar);
        Intent intent = getIntent();
        mArtistId = intent.getStringExtra("artistId");
        mPresenter.onViewInitialized(mArtistId);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.profile)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.artworks)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.related)));

        mViewPager.setOffscreenPageLimit(mTabLayout.getTabCount());

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

}
