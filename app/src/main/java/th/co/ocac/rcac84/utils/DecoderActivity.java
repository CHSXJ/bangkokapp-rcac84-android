package th.co.ocac.rcac84.utils;

import android.Manifest;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import th.co.ocac.rcac84.R;

import th.co.ocac.rcac84.ui.main.MainMvpPresenter;
import th.co.ocac.rcac84.ui.main.MainMvpView;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import javax.inject.Inject;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class DecoderActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private TextView resultTextView;
    private QRCodeReaderView qrCodeReaderView;

    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decoder);

        qrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
        qrCodeReaderView.setBackCamera();
        qrCodeReaderView.setOnQRCodeReadListener(this);
        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);

        // Use this function to change the autofocus interval (default is 5 secs)
        qrCodeReaderView.setAutofocusInterval(2000L);

        // Use this function to enable/disable Torch
        qrCodeReaderView.setTorchEnabled(true);

        resultTextView = (TextView) findViewById(R.id.exampleTextView);

        ImageView line_image = (ImageView) findViewById(R.id.red_line_image);

        TranslateAnimation mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f);
        mAnimation.setDuration(1000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        line_image.setAnimation(mAnimation);

        DecoderActivityPermissionsDispatcher.openCameraWithCheck(this);
    }

    // Called when a QR is decoded
    // "text" : the text encoded in QR
    // "points" : points where QR control points are placed in View
    @Override
    public void onQRCodeRead(String text, PointF[] points) {

        String[] type = text.split("\\.");
        resultTextView.setText(type[0]);

//        if (type[0].equals("EVENT")) {
//            startActivity(EventDetailActivity.getStartIntent(this, type[1]));
//            mPresenter.joinEvent(type[1]);
            Intent data = new Intent();
            data.putExtra("type", type[0]);
            data.putExtra("id",type[1]);
            setResult(RESULT_OK, data);
            finish();
//        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        openCamera();
    }

    @NeedsPermission(Manifest.permission.CAMERA)
    public void openCamera() {
        qrCodeReaderView.startCamera();
    }


    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
