package th.co.ocac.rcac84.data.network;

import android.util.Log;

import th.co.ocac.rcac84.data.network.model.Artist;
import th.co.ocac.rcac84.data.network.model.Artwork;
import th.co.ocac.rcac84.data.network.model.Event;
import th.co.ocac.rcac84.data.network.model.JoinResponse;
import th.co.ocac.rcac84.data.network.model.LoginRequest;
import th.co.ocac.rcac84.data.network.model.LoginResponse;
import th.co.ocac.rcac84.data.network.model.LogoutResponse;
import th.co.ocac.rcac84.data.network.model.Museum;
import th.co.ocac.rcac84.data.network.model.News;
import th.co.ocac.rcac84.data.network.model.OpenSourceResponse;
import th.co.ocac.rcac84.data.network.model.SearchResponse;
import th.co.ocac.rcac84.data.network.model.Tag;
import th.co.ocac.rcac84.data.network.model.User;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by Ch on 10/10/18.
 */

@Singleton
public class AppApiHelper implements ApiHelper {

    private static final String TAG = "AppApiHelper";
    private ApiHeader mApiHeader;

    @Inject
    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }

    @Override
    public Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest
                                                                request) {
        Log.d(TAG, "doFacebookLoginApiCall");
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_FACEBOOK_LOGIN)
                .addQueryParameter("token", request.getFbAccessToken())
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest
                                                              request) {
        Log.d(TAG, "doServerLoginApiCall");
        Log.d(TAG, request.getEmail());
        Log.d(TAG, String.valueOf(request.hashCode()));
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
//                .addBodyParameter(request)
                .addApplicationJsonBody(request)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<LogoutResponse> doLogoutApiCall() {
        Log.d(TAG, "doLogoutApiCall");
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_LOGOUT)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(LogoutResponse.class);
    }

    @Override
    public Observable<List<Event>> getCurrentEventsApiCall() {
        Log.d(TAG, "getCurrentEventsApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_EVENTS_NOW)
//                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectListObservable(Event.class);
    }

    @Override
    public Observable<List<Event>> getNextEventsApiCall() {
        Log.d(TAG, "getNextEventsApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_EVENTS_NEXT)
//                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectListObservable(Event.class);
    }

    @Override
    public Single<OpenSourceResponse> getOpenSourceApiCall() {
        Log.d(TAG, "getOpenSourceApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_EVENTS_NOW)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(OpenSourceResponse.class);
    }

    @Override
    public Observable<List<Artwork>> getArtOfTheMonthApiCall() {
        Log.d(TAG, "getArtOfTheMonthApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_ART_OF_THE_MONTH)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectListObservable(Artwork.class);
    }

    @Override
    public Observable<List<Artwork>> getGalleryByTagsApiCall(List<String> tagIds) {
        Log.d(TAG, "getGalleryByTagsApiCall");
        String q = tagIds.toString();
        q = q.substring(1, q.length()-1);
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GALLERY_BY_TAGS)
                .addQueryParameter("id", q)
                .build()
                .getObjectListObservable(Artwork.class);
    }

    @Override
    public Single<Event> getEventDetail(String eventId) {
        Log.d(TAG, "getEventDetail");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_EVENT_DETAIL)
                .addPathParameter("id", eventId)
                .build()
                .getObjectSingle(Event.class);
    }

    @Override
    public Single<Artwork> getArtworkDetail(String mArtworkId) {
        Log.d(TAG, "getArtworkDetail");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_ARTWORK_DETAIL)
                .addPathParameter("id", mArtworkId)
                .build()
                .getObjectSingle(Artwork.class);
    }

    @Override
    public Single<Artist> getArtistDetail(String mArtistId) {
        Log.d(TAG, "getArtistDetail");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_ARTIST_DETAIL)
                .addPathParameter("id", mArtistId)
                .build()
                .getObjectSingle(Artist.class);
    }

    @Override
    public Observable<List<Artwork>> getArtworksByArtistIdApiCall(String artistId) {
        Log.d(TAG, "getArtworksByArtistIdApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_ARTWORKS_BY_ARTISTID)
                .addPathParameter("id", artistId)
                .build()
                .getObjectListObservable(Artwork.class);
    }

    @Override
    public Single<User> getUserProfileApiCall() {
        Log.d(TAG, "getUserProfileApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_USER)
                .addHeaders("Authorization", "Bearer " + mApiHeader.getProtectedApiHeader().getAccessToken())
                .build()
                .getObjectSingle(User.class);
    }

    @Override
    public Observable<List<Event>> getUserPreviousJoinedApiCall(String userId) {
        Log.d(TAG, "getUserPreviousJoinedApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_USER_JOINED_EVENTS)
                .addHeaders("Authorization", "Bearer " + mApiHeader.getProtectedApiHeader().getAccessToken())
                .addQueryParameter("userId", userId)
                .build()
                .getObjectListObservable(Event.class);
    }

    @Override
    public Single<String> getSaveProfileApiCall(User user) {
        Log.d(TAG, "getSaveProfileApiCall");
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SAVE_PROFILE)
                .addHeaders("Authorization", "Bearer " + mApiHeader.getProtectedApiHeader().getAccessToken())
                .addApplicationJsonBody(user)
                .build()
                .getStringSingle();
    }

    @Override
    public Single<JoinResponse> getJoinEventApiCall(String eventId, String userId) {
        Log.d(TAG, "getJoinEventApiCall");
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_JOIN_EVENT)
                .addHeaders("Authorization", "Bearer " + mApiHeader.getProtectedApiHeader().getAccessToken())
                .addPathParameter("id", eventId)
                .addPathParameter("userId", userId)
                .build()
                .getObjectSingle(JoinResponse.class);
    }

    @Override
    public Single<SearchResponse> getSearchApiCall(String s) {
        Log.d(TAG, "getSearchApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SEARCH)
                .addQueryParameter("q", s)
                .build()
                .getObjectSingle(SearchResponse.class);
    }

    @Override
    public Observable<List<News>> getNewsApiCall() {
        Log.d(TAG, "getNewsApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_NEWS)
                .build()
                .getObjectListObservable(News.class);
    }

    @Override
    public Single<Museum> getMuseumApiCall(String s) {
        Log.d(TAG, "getMuseumApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_MUSEUM)
                .addPathParameter("id", s)
                .build()
                .getObjectSingle(Museum.class);
    }

    @Override
    public Observable<List<Tag>> getAllTagsApiCall() {
        Log.d(TAG, "getAllTagsApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_ALL_TAGS)
                .build()
                .getObjectListObservable(Tag.class);
    }

    @Override
    public Single<String> getRegisterProfileApiCall(User user) {
        Log.d(TAG, "getRegisterProfileApiCall");
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_REGISTER)
                .addApplicationJsonBody(user)
                .build()
                .getStringSingle();
    }

    @Override
    public Single<SearchResponse> getArtistsRelatedApiCall(String artistId) {
        Log.d(TAG, "getArtistsRelatedApiCall");
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_ARTIST_RELATED)
                .addPathParameter("id", artistId)
                .build()
                .getObjectSingle(SearchResponse.class);
    }

}

