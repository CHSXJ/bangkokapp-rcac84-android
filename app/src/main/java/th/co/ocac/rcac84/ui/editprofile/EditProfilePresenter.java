package th.co.ocac.rcac84.ui.editprofile;

import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.User;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Ch on 10/10/18.
 */

public class EditProfilePresenter<V extends EditProfileMvpView> extends BasePresenter<V>
        implements EditProfileMvpPresenter<V> {

    private static final String TAG = "EditProfilePresenter";

    @Inject
    public EditProfilePresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    public void getUserProfile() {
        getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager()
            .getUserProfileApiCall()
            .subscribeOn(getSchedulerProvider().io())
            .observeOn(getSchedulerProvider().ui())
            .subscribe(new Consumer<User>() {
                @Override
                public void accept(User user) throws Exception {
                    getDataManager().updateUserInfo(user, DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_IN);
                    getMvpView().openMainActivity();
                }
            }, new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {

                }
            }));
    }

    @Override
    public void getLoggedInProfile() {
        getMvpView().displayUser(getDataManager().getUserInfo());
    }

    @Override
    public void saveProfile(User user) {

        getMvpView().showLoading();
        user.setId("");

        getCompositeDisposable().add(getDataManager()
            .getSaveProfileApiCall(user)
            .subscribeOn(getSchedulerProvider().io())
            .observeOn(getSchedulerProvider().ui())
            .subscribe(new Consumer<String>() {
                @Override
                public void accept(String s) throws Exception {
                    System.out.println("s = " + s);
                    getUserProfile();
                }
            }, new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {
                    System.out.println("throwable = " + throwable);
                }
            }));
    }
}
