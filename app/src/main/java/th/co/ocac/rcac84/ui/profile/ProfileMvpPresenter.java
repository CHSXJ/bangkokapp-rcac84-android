package th.co.ocac.rcac84.ui.profile;

import th.co.ocac.rcac84.di.PerActivity;
import th.co.ocac.rcac84.ui.base.MvpPresenter;

/**
 * Created by Ch on 16/10/18.
 */

@PerActivity
public interface ProfileMvpPresenter<V extends ProfileMvpView> extends MvpPresenter<V> {

    void getLoggedInProfile();

    void onSignout();
}
