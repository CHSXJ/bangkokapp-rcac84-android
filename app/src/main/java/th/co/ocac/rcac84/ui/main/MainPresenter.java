package th.co.ocac.rcac84.ui.main;

import com.androidnetworking.error.ANError;
import th.co.ocac.rcac84.data.DataManager;
import th.co.ocac.rcac84.data.network.model.JoinResponse;
import th.co.ocac.rcac84.data.network.model.LogoutResponse;
import th.co.ocac.rcac84.ui.base.BasePresenter;
import th.co.ocac.rcac84.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


/**
 * Created by Ch on 11/10/18.
 */

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {

    private static final String TAG = "MainPresenter";

    @Inject
    public MainPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onDrawerOptionAboutClick() {
        getMvpView().closeNavigationDrawer();
        getMvpView().showAboutFragment();
    }

    @Override
    public void onDrawerOptionLogoutClick() {
        getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager().doLogoutApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<LogoutResponse>() {
                    @Override
                    public void accept(LogoutResponse response) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getDataManager().setUserAsLoggedOut();
                        getMvpView().hideLoading();
                        getMvpView().openLoginActivity();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the login error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));

    }

    @Override
    public void onViewInitialized() {
    }

    @Override
    public void joinEvent(final String eventId) {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getJoinEventApiCall(eventId, getDataManager().getUserInfo().getId())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<JoinResponse>() {
                    @Override
                    public void accept(JoinResponse joinResponse) throws Exception {
                        getMvpView().showJoinAlert(eventId, "Complete!, successfully join this event");
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getMvpView().showJoinAlert(eventId, "You've already registered");
                    }
                }));
    }

    @Override
    public void onNavMenuCreated() {
        if (!isViewAttached()) {
            return;
        }
        getMvpView().updateAppVersion();

    }

    @Override
    public void checkUserLoggedIn() {
        if(getDataManager().getCurrentUserLoggedInMode()
                == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType()) {
            getMvpView().hideSignInMenu(false);
        } else {
            getMvpView().hideSignInMenu(true);
        }
    }

    @Override
    public String getUserToken() {
        if (getDataManager().getAccessToken() != null) {
            return getDataManager().getAccessToken();
        } else {
            return null;
        }
    }

    @Override
    public void onDrawerNewsClick() {
        getMvpView().closeNavigationDrawer();
        getMvpView().openNewsActivity();
    }

    @Override
    public void onDrawerLocationClick() {
        getMvpView().closeNavigationDrawer();
        getMvpView().openLocationActivity();
    }
}
