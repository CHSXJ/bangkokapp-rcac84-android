package th.co.ocac.rcac84.ui.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import th.co.ocac.rcac84.BuildConfig;
import th.co.ocac.rcac84.R;
import th.co.ocac.rcac84.ui.about.AboutActivity;
import th.co.ocac.rcac84.ui.artist.ArtistDetailActivity;
import th.co.ocac.rcac84.ui.artworkdetail.ArtworkDetailActivity;
import th.co.ocac.rcac84.ui.base.BaseActivity;
import th.co.ocac.rcac84.ui.custom.RoundedImageView;
import th.co.ocac.rcac84.ui.eventdetail.EventDetailActivity;
import th.co.ocac.rcac84.ui.location.LocationActivity;
import th.co.ocac.rcac84.ui.news.NewsActivity;
import th.co.ocac.rcac84.ui.login.LoginActivity;
import th.co.ocac.rcac84.ui.profile.ProfileActivity;
import th.co.ocac.rcac84.utils.DecoderActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ch on 16/10/18.
 */

public class MainActivity extends BaseActivity implements MainMvpView {

    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    @Inject
    MainPagerAdapter mPagerAdapter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;

    @BindView(R.id.drawer_view)
    DrawerLayout mDrawer;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    @BindView(R.id.tv_app_version)
    TextView mAppVersionTextView;

    @BindView(R.id.feed_view_pager)
    ViewPager mViewPager;

    private TextView mNameTextView;

    private TextView mEmailTextView;

    private RoundedImageView mProfileImageView;

    private ActionBarDrawerToggle mDrawerToggle;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        setUp();
    }

    @Override
    public void updateAppVersion() {
        String version = getString(R.string.version) + " " + BuildConfig.VERSION_NAME;
        mAppVersionTextView.setText(version);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDrawer != null)
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        mPresenter.checkUserLoggedIn();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onFragmentAttached() {
    }

    @Override
    public void onFragmentDetached(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                    .remove(fragment)
                    .commitNow();
            unlockDrawer();
        }
    }

    @Override
    public void showAboutFragment() {
        lockDrawer();
//        getSupportFragmentManager()
//                .beginTransaction()
//                .disallowAddToBackStack()
//                .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
//                .add(R.id.cl_root_view, AboutFragment.newInstance(), AboutFragment.TAG)
//                .commit();
        startActivity(AboutActivity.getStartIntent(getApplicationContext()));
    }

    @Override
    public void lockDrawer() {
        if (mDrawer != null)
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void unlockDrawer() {
        if (mDrawer != null)
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            case R.id.action_scan:
                Intent intent = new Intent(getApplicationContext(), DecoderActivity.class);
                startActivityForResult(intent, 99);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 99) {
            if (resultCode == RESULT_OK) {

                String type = data.getStringExtra("type");
                String id = data.getStringExtra("id");
                if(type.equals("EVENT")) {

                    if (mPresenter.getUserToken() != null) {
                        mPresenter.joinEvent(id);
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.setTitle("Before joining this event, please sign in first");
                        alert.setPositiveButton("Sign in", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(LoginActivity.getStartIntent(getApplicationContext()));
                            }
                        });
                        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        alert.show();
                    }
                } else if (type.equals("ARTWORK")) {
                    startActivity(ArtworkDetailActivity.getStartIntent(this, id));
                } else if (type.equals("ARTIST")) {
                    startActivity(ArtistDetailActivity.getStartIntent(this, id));
                }
            }
        }
    }

    @Override
    public void showJoinAlert(final String eventId, String msg) {
        hideLoading();

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(msg);
        alert.setPositiveButton("See Event", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(EventDetailActivity.getStartIntent(getApplicationContext(), eventId));
//                openEventDetail(eventId);
            }
        });
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alert.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void setUp() {
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawer,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mToolbar.setForegroundGravity(Gravity.RIGHT);
//        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if (mDrawer.isDrawerOpen(Gravity.RIGHT)) {
//                    mDrawer.closeDrawer(Gravity.RIGHT);
//                } else {
//                    mDrawer.openDrawer(Gravity.RIGHT);
//                }
//            }
//        });
        setupNavMenu();
        mPresenter.onNavMenuCreated();
//        setupCardContainerView();
        mPresenter.onViewInitialized();

        //add pagerx
        mPagerAdapter.setCount(4);

        mViewPager.setAdapter(mPagerAdapter);

        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.whatsnews)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.events)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.gallery)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.search)));

        mViewPager.setOffscreenPageLimit(mTabLayout.getTabCount());

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    void setupNavMenu() {
        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        mDrawer.closeDrawer(Gravity.START);
                        switch (item.getItemId()) {
                            case R.id.nav_item_search:
                                mViewPager.setCurrentItem(3, true);
                                return true;
                            case R.id.nav_item_events:
                                mViewPager.setCurrentItem(1, true);
                                return true;
                            case R.id.nav_item_gallery:
                                mViewPager.setCurrentItem(2, true);
                                return true;
                            case R.id.nav_item_news:
                                mPresenter.onDrawerNewsClick();
                                return true;
                            case R.id.nav_item_location:
                                mPresenter.onDrawerLocationClick();
                                return true;
                            case R.id.nav_item_about:
                                mPresenter.onDrawerOptionAboutClick();
                                return true;
                            case R.id.nav_item_signin:
                                openLoginActivity();
                                return true;
                            case R.id.nav_item_profile:
                                openProfileActivity();
                            default:
                                return false;
                        }
                    }
                });
        mPresenter.checkUserLoggedIn();
    }

    @Override
    public void hideSignInMenu(boolean hide) {
        if (hide == true) {
            mNavigationView.getMenu().findItem(R.id.nav_item_profile).setVisible(true);
            mNavigationView.getMenu().findItem(R.id.nav_item_signin).setVisible(false);
        } else {
            mNavigationView.getMenu().findItem(R.id.nav_item_profile).setVisible(false);
            mNavigationView.getMenu().findItem(R.id.nav_item_signin).setVisible(true);
        }
    }

    @Override
    public void openLoginActivity() {
        startActivity(LoginActivity.getStartIntent(this));
    }

    @Override
    public void openLocationActivity() {
        startActivity(LocationActivity.getStartIntent(this));
    }

    private void openProfileActivity() {
        startActivity(ProfileActivity.getStartIntent(this));
    }

    @Override
    public void openNewsActivity() {
        startActivity(NewsActivity.getStartIntent(this));
    }

    @Override
    public void closeNavigationDrawer() {
        if (mDrawer != null) {
            mDrawer.closeDrawer(Gravity.START);
        }
    }
}
