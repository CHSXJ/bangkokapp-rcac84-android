package th.co.ocac.rcac84.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import th.co.ocac.rcac84.ui.allevents.AllEventsFragment;
import th.co.ocac.rcac84.ui.gallery.GalleryFragment;
import th.co.ocac.rcac84.ui.search.SearchFragment;
import th.co.ocac.rcac84.ui.whatsnew.WhatsnewFragment;

/**
 * Created by Ch on 21/07/2018.
 */

public class MainPagerAdapter extends FragmentStatePagerAdapter {

    private int mTabCount;

    public MainPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.mTabCount = 0;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return WhatsnewFragment.newInstance();
            case 1:
                return AllEventsFragment.newInstance();
            case 2:
                return GalleryFragment.newInstance();
            case 3:
                return SearchFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mTabCount;
    }

    public void setCount(int count) {
        mTabCount = count;
    }
}
