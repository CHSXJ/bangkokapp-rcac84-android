/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package th.co.ocac.rcac84.data.network;

import th.co.ocac.rcac84.BuildConfig;

/**
 * Created by amitshekhar on 01/02/17.
 */

public final class ApiEndPoint {

    public static final String ENDPOINT_GOOGLE_LOGIN = BuildConfig.BASE_URL;
//            + "/588d14f4100000a9072d2943";

    public static final String ENDPOINT_FACEBOOK_LOGIN = BuildConfig.BASE_URL
            + "/api/fb/authenticate";

    public static final String ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL
            + "/api/authenticate";

    public static final String ENDPOINT_LOGOUT = BuildConfig.BASE_URL;
//            + "/588d161c100000a9072d2946";

    public static final String ENDPOINT_BLOG = BuildConfig.BASE_URL;
//            + "/5926ce9d11000096006ccb30";

    public static final String ENDPOINT_EVENTS_NOW = BuildConfig.BASE_URL
            + "/api/m/events?period=NOW";

    public static final String ENDPOINT_EVENTS_NEXT = BuildConfig.BASE_URL
            + "/api/m/events?period=NEXT";

    public static final String ENDPOINT_ART_OF_THE_MONTH = BuildConfig.BASE_URL
            + "/api/m/artworks/artOfMonth";

    public static final String ENDPOINT_GALLERY_BY_TAGS = BuildConfig.BASE_URL
            + "/api/m/artworks/tags";

    public static final String ENDPOINT_EVENT_DETAIL = BuildConfig.BASE_URL
            + "/api/m/events/{id}";

    public static final String ENDPOINT_ARTWORK_DETAIL = BuildConfig.BASE_URL
            + "/api/m/artworks/{id}";

    public static final String ENDPOINT_ARTIST_DETAIL = BuildConfig.BASE_URL
            + "/api/m/artists/{id}";

    public static final String ENDPOINT_ARTWORKS_BY_ARTISTID = BuildConfig.BASE_URL
            + "/api/m/artists/{id}/artworks";

    public static final String ENDPOINT_ARTIST_RELATED = BuildConfig.BASE_URL
            + "/api/m/artists/{id}/relates";

    public static final String ENDPOINT_GET_USER = BuildConfig.BASE_URL
            +"/api/account";

    public static final String ENDPOINT_GET_USER_JOINED_EVENTS = BuildConfig.BASE_URL
            +"/api/m/visitors";

    public static final String ENDPOINT_SAVE_PROFILE = BuildConfig.BASE_URL
            +"/api/account";

    public static final String ENDPOINT_JOIN_EVENT = BuildConfig.BASE_URL
            +"/api/m/events/{id}/audiences/{userId}";

    public static final String ENDPOINT_SEARCH = BuildConfig.BASE_URL
            +"/api/m/search/";

    public static final String ENDPOINT_NEWS = BuildConfig.BASE_URL
            +"/api/m/news/";

    public static final String ENDPOINT_MUSEUM = BuildConfig.BASE_URL
            +"/api/m/museums/{id}";

    public static final String ENDPOINT_ALL_TAGS = BuildConfig.BASE_URL
            +"/api/m/tags";

    public static final String ENDPOINT_REGISTER = BuildConfig.BASE_URL
            +"/api/register";

}
